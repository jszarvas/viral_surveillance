#!/usr/bin/env python3

import os
import sys
import argparse
import json
import sqlite3
import requests
import shlex
import subprocess
import multiprocessing
import glob
import shutil
import time
import vu_service

class ENA_connection:
    def __init__(self, url_user, auth_required):
        self.url = url_user
        self.auth = auth_required
        self.accession = "accession"
        self.description = "description"
        self.recordpath = "accession"
        self.set_access_mode()
        self.set_minimum_results()
        #print(self.url)
        self.get_search()
        #print(self.response.status_code)

    def set_access_mode(self):
        if self.auth:
            # search only the data hub
            if self.url.find("dccDataOnly") == -1:
                self.url = "{}&dccDataOnly=true".format(self.url)
                self.url = self.url.replace("dataPortal=ena", "dataPortal=pathogen")

    def set_minimum_results(self):
        accession_levels = {"read_run": "run_accession",
                            "analysis": "analysis_accession",
                            "read_experiment": "experiment_accession"}
        description_levels = {"read_run": "experiment_title",
                            "analysis": "sample_accession",
                            "read_experiment": "experiment_title"}
        recordpath_levels= {"read_run": "fastq_ftp",
                            "analysis": "submitted_ftp",
                            "read_experiment": "fastq_ftp"}

        # if other accession type, etc. is returned, use that one
        for result_type in accession_levels.keys():
            if self.url.find("result={}".format(result_type)) != -1:
                self.accession = accession_levels[result_type]
                self.description = description_levels[result_type]
                self.recordpath = recordpath_levels[result_type]

        self.minimum_fields = [self.description, "collection_date", "country", self.recordpath]
        # check the url to contain the minimum fields needed
        if self.url.find("fields=") != -1:
            field_to_add = []
            for field in self.minimum_fields:
                if self.url.find(field) == -1:
                    field_to_add.append(field)
            if field_to_add:
                tmp = self.url.split("&fields=")
                self.url = tmp[0] + "&fields={},".format(",".join(field_to_add)) + tmp[1]

        else:
            # the whole parameter is missing
            self.url += "&fields={}".format(",".join(self.minimum_fields))

    def get_search(self):
        if not self.auth:
            self.response = requests.get(self.url)
        else:
            # authenticated API access
            self.response = requests.get(self.url, auth=(os.environ['DCC_USER'], os.environ['DCC_PWD']))


def get_subfolder(data_parent_dir, clean=False):
    """Avoid having more than 10000 files in one subfolder"""
    olddir = os.getcwd()
    os.chdir(data_parent_dir)
    ptrn = "data-[0-9]*"
    dts = glob.glob(ptrn)
    os.chdir(olddir)
    # no subfolder exist yet
    if not dts:
        subfolder = os.path.join(data_parent_dir, "data-{:04d}".format(1))
    else:
        dts.sort()
        subfolder = os.path.join(data_parent_dir, dts[-1])
        if len(os.listdir(subfolder)) > 10000:
            # create the next one
            i = int(dts[-1].split("-")[-1]) + 1
            subfolder = os.path.join(data_parent_dir, "data-{:04d}".format(i))
            if clean:
                # remove the full folder
                old_subfolder = os.path.join(data_parent_dir, dts[-1])
                try:
                    shutil.rmtree(old_subfolder)
                except:
                    vu_service.exiting("{} directory cannot be removed".format(old_subfolder), notify=True)
    if not os.path.exists(subfolder):
        os.mkdir(subfolder)
    return subfolder

def minimum_metadata(ena_record, enforce=True):
    """Require both collection date and location"""
    if enforce and (not ena_record["collection_date"] or not ena_record["country"]):
        return False
    return True

def download_command(result_type, ftp_path, local_path=None, authenticate=False):
    """Construct download command for given user access method and result type"""
    # FASTA_BASE = "wget --quiet --timeout=30 --tries=3 --waitretry=2 -O {0} https://www.ebi.ac.uk/ena/browser/api/fasta/{1}?download=True"
    # FASTQ_BASE = "wget --quiet --timeout=30 --tries=3 --waitretry=2 --continue --directory-prefix={0} ftp://{1}"

    WGET_BASE = "wget --quiet --timeout=30 --tries=3 --waitretry=2 --retry-connrefused"
    ftp_server = "ftp://ftp.sra.ebi.ac.uk"

    cmd_items = [WGET_BASE]
    if authenticate:
        # use FTPS (with fall back to ftp) and get credentials from env variables
        ftp_server = "ftps://ftp.dcc-private.ebi.ac.uk"
        cmd_items.append("--ftps-fallback-to-ftp")
        cmd_items.append("--user ${DCC_USER}")
        cmd_items.append("--password ${DCC_PWD}")

    if result_type == "public_fasta":
        cmd_items.append("-O {0} https://www.ebi.ac.uk/ena/browser/api/fasta/{1}?download=True".format(local_path, ftp_path))
    elif result_type == "fastq":
        ftp_path = ftp_path.split("/", 1)[1]
        cmd_items.append("--continue")
        cmd_items.append("--directory-prefix={0} {2}/{1}".format(local_path, ftp_path, ftp_server))
    elif result_type == "ena_analysis":
        ftp_path = ftp_path.split("/", 1)[1]
        cmd_items.append("--continue")
        cmd_items.append("-O {0} {2}/{1}".format(local_path, ftp_path, ftp_server))
    else:
        return None

    #print(" ".join(cmd_items))
    return " ".join(cmd_items)

def download_loop(record):
    """Loop around wget command"""
    commands = record[0]
    local_files = record[1][1].split(",")
    for i, cmd in enumerate(commands):
        tries = 3
        ex = 1
        while ex and tries > 0:
            ex = vu_service.call_shell(cmd)
            if ex:
                tries -= 1
                time.sleep(5)
        # check existence and size
        if ex or not os.path.exists(local_files[i]) or not os.path.getsize(local_files[i]) > 0:
            return None
    return record[1:]

parser = argparse.ArgumentParser(
    description='Prepares ENA samples for processing')
parser.add_argument(
    '-u',
    dest="query_url",
    default="https://www.ebi.ac.uk/ena/portal/api/search?dataPortal=ena&query=tax_tree(2697049)&result=sequence&fields=collection_date,country,description&format=json",
    help='Url for Portal API search')
parser.add_argument(
    '-o',
    dest="data_folder",
    help='Path to data folder for fasta/fastq')
parser.add_argument(
    '-d',
    dest="database",
    help="Sample database"
)
parser.add_argument(
    '-p',
    dest="parallel",
    type=int,
    default=2,
    help="Number of parallel processes for download"
)
parser.add_argument(
    '-l',
    dest="limit",
    type=int,
    default=0,
    help="Limit number of samples added"
)
parser.add_argument(
    '-s',
    dest="save_cycle",
    type=int,
    default=20,
    help="Save to database every X sequences"
)
parser.add_argument(
    '-m',
    dest="loose_metadata",
    action="store_true",
    help="Download records without minimum metadata"
)
parser.add_argument(
    '-a',
    dest="auth",
    action="store_true",
    help="Access private data hub, credentials supplied in DCC_USER and DCC_PWD env. variables"
)
parser.add_argument(
    '--clean',
    dest="clean",
    action="store_true",
    help="Remove the previous data-000N folder when starting a new"
)
args = parser.parse_args()

# init timer
t0 = time.time()

#start up the db during first run_id
conn = sqlite3.connect(args.database)
conn.execute("PRAGMA foreign_keys = 1")
cur = conn.cursor()

cur.execute('''CREATE TABLE IF NOT EXISTS samples
    (accession TEXT PRIMARY KEY,
    path TEXT,
    feature_path TEXT,
    dl_date TEXT DEFAULT CURRENT_DATE,
    included INTEGER DEFAULT NULL);''')
cur.execute('''CREATE TABLE IF NOT EXISTS metadata
    (accession TEXT PRIMARY KEY,
    description TEXT,
    submit_date TEXT,
    country TEXT);''')
conn.commit()

data_subfolder = get_subfolder(args.data_folder, clean=args.clean)

strictness = True
if args.loose_metadata:
    strictness = False

# make the GET search Portal API
extracted_record_path = None
entries = []
valid_items = 0
failed_items = 0

ena_conn = ENA_connection(args.query_url, args.auth)
if ena_conn.response.status_code == 200 and ena_conn.response.json():
    for item in ena_conn.response.json():
        cur.execute('''SELECT * FROM samples WHERE accession=?''', (item[ena_conn.accession],))
        if cur.fetchone() is None and minimum_metadata(item, strictness):
            unit = []
            wget_cmd = []
            record_path = None
            if ena_conn.accession == "run_accession" and item[ena_conn.recordpath]:
                # raw reads
                tmp = []
                raw_files = item[ena_conn.recordpath].split(";")
                if len(raw_files) == 3:
                    # throw away the single reads
                    raw_files = raw_files[1:]
                for raw_read in raw_files:
                    fastq_path = os.path.join(data_subfolder, os.path.basename(raw_read))
                    tmp.append(fastq_path)
                    wget_cmd.append(download_command("fastq", raw_read, data_subfolder, args.auth))
                record_path = ",".join(tmp)
            elif ena_conn.accession == "analysis_accession" and item[ena_conn.recordpath]:
                analysis_files = item[ena_conn.recordpath].split(";")
                for analysis_file in analysis_files:
                    if analysis_file.find(".fasta.gz") != -1:
                        record_path = os.path.join(data_subfolder, "{}.fasta.gz".format(item[ena_conn.accession]))
                        wget_cmd.append(download_command("ena_analysis", analysis_file, record_path, args.auth))
            elif ena_conn.accession == "accession":
                record_path = os.path.join(data_subfolder, "{}.fasta".format(item[ena_conn.accession]))
                wget_cmd.append(download_command("public_fasta", item[ena_conn.accession], record_path))
            else:
                pass

            if wget_cmd:
                # construct download unit
                unit.append(wget_cmd)
                # sample_id    pathtoread1,pathtoread2
                unit.append((item[ena_conn.accession], record_path, extracted_record_path))
                unit.append((item[ena_conn.accession],item[ena_conn.description], item["collection_date"], item["country"]))
                entries.append(unit)
                valid_items += 1

            if valid_items and valid_items % args.save_cycle == 0:
                if __name__ == '__main__':

                    sample_insert = []
                    metadata_insert = []
                    pool = multiprocessing.Pool(args.parallel)
                    for response in pool.imap_unordered(download_loop, entries):
                        if response is not None:
                            sample_insert.append(response[0])
                            metadata_insert.append(response[1])
                        else:
                            failed_items += 1
                    pool.close()
                    pool.join()

                if sample_insert:
                    # insert to db
                    cur.executemany('''INSERT OR REPLACE INTO samples (accession, path, feature_path) VALUES (?,?,?);''', sample_insert)
                    conn.commit()

                    cur.executemany('''INSERT OR IGNORE INTO metadata (accession, description, submit_date, country) VALUES (?,?,?,?);''', metadata_insert)
                    conn.commit()

                # clean entries
                entries = []

        if args.limit and (valid_items - failed_items) == args.limit:
            break

    # download remaining entries (difference between cycle and limit)
    if entries:
        sample_insert = []
        metadata_insert = []
        for unit in entries:
            response = download_loop(unit)
            if response is not None:
                sample_insert.append(response[0])
                metadata_insert.append(response[1])
            else:
                failed_items += 1
        if sample_insert:
            # insert to db
            cur.executemany('''INSERT OR REPLACE INTO samples (accession, path, feature_path) VALUES (?,?,?);''', sample_insert)
            conn.commit()

            cur.executemany('''INSERT OR IGNORE INTO metadata (accession, description, submit_date, country) VALUES (?,?,?,?);''', metadata_insert)
            conn.commit()
    conn.close()
else:
    conn.close()
    vu_service.exiting("Error: {} {}".format(ena_conn.response.status_code, args.query_url), notify=True)

vu_service.timing(t0, "# {} Samples and metadata saved to database".format(valid_items - failed_items))
