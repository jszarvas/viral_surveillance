#!/usr/bin/env python3
# -*- coding: utf-8 -*-
countries_hash ={"Canada": {"latitude": "45.4167", "city": "Ottawa", "longitude": "-75.7000", "country": "Canada"}, "Brazil": {"latitude": "-15.7833", "city": "Brasília", "longitude": "-47.9161", "country": "Brazil"}, "Fiji": {"latitude": "-18.1330", "city": "Suva", "longitude": "178.4417", "country": "Fiji"}, "Turkmenistan": {"latitude": "37.9500", "city": "Ashgabat", "longitude": "58.3833", "country": "Turkmenistan"}, "Democratic Republic of Congo": {"latitude": "-4.3297", "city": "Kinshasa", "longitude": "15.3150", "country": "Democratic Republic of Congo"}, "Lithuania": {"latitude": "54.6834", "city": "Vilnius", "longitude": "25.3166", "country": "Lithuania"}, "Bahamas": {"latitude": "25.0834", "city": "Nassau", "longitude": "-77.3500", "country": "Bahamas"}, "Ethiopia": {"latitude": "9.0333", "city": "Addis Ababa", "longitude": "38.7000", "country": "Ethiopia"}, "Aruba": {"latitude": "12.5304", "city": "Oranjestad", "longitude": "-70.0290", "country": "Aruba"}, "Swaziland": {"latitude": "-26.4667", "city": "Lobamba", "longitude": "31.2000", "country": "Swaziland"}, "Guinea-Bissau": {"latitude": "11.8650", "city": "Bissau", "longitude": "-15.5984", "country": "Guinea-Bissau"}, "Argentina": {"latitude": "-34.6025", "city": "Buenos Aires", "longitude": "-58.3975", "country": "Argentina"}, "Bolivia": {"latitude": "-19.0410", "city": "Sucre", "longitude": "-65.2595", "country": "Bolivia"}, "Cameroon": {"latitude": "3.8667", "city": "Yaounde", "longitude": "11.5167", "country": "Cameroon"}, "Burkina Faso": {"latitude": "12.3703", "city": "Ouagadougou", "longitude": "-1.5247", "country": "Burkina Faso"}, "Bahrain": {"latitude": "26.2361", "city": "Manama", "longitude": "50.5831", "country": "Bahrain"}, "Saudi Arabia": {"latitude": "24.6408", "city": "Riyadh", "longitude": "46.7727", "country": "Saudi Arabia"}, "Yemen": {"latitude": "15.3547", "city": "Sanaa", "longitude": "44.2066", "country": "Yemen"}, "Micronesia": {"latitude": "6.9166", "city": "Palikir", "longitude": "158.1500", "country": "Micronesia"}, "Japan": {"latitude": "35.6850", "city": "Tokyo", "longitude": "139.7514", "country": "Japan"}, "Wallis & Futuna": {"latitude": "-13.2825", "city": "Mata-Utu", "longitude": "-176.1736", "country": "Wallis & Futuna"}, "American Samoa": {"latitude": "-14.2740", "city": "Pago Pago", "longitude": "-170.7046", "country": "American Samoa"}, "Northern Mariana Islands": {"latitude": "15.2137", "city": "Capitol Hill", "longitude": "145.7546", "country": "Northern Mariana Islands"}, "Slovenia": {"latitude": "46.0553", "city": "Ljubljana", "longitude": "14.5150", "country": "Slovenia"}, "Guatemala": {"latitude": "14.6211", "city": "Guatemala", "longitude": "-90.5270", "country": "Guatemala"}, "Zimbabwe": {"latitude": "-17.8178", "city": "Harare", "longitude": "31.0447", "country": "Zimbabwe"}, "Kuwait": {"latitude": "29.3697", "city": "Kuwait", "longitude": "47.9783", "country": "Kuwait"}, "Jordan": {"latitude": "31.9500", "city": "Amman", "longitude": "35.9333", "country": "Jordan"}, "Saint Barthelemy": {"latitude": "17.8958", "city": "Gustavia", "longitude": "-62.8508", "country": "Saint Barthelemy"}, "Dominica": {"latitude": "15.3010", "city": "Roseau", "longitude": "-61.3870", "country": "Dominica"}, "Liberia": {"latitude": "6.3106", "city": "Monrovia", "longitude": "-10.8048", "country": "Liberia"}, "Netherlands": {"latitude": "52.0800", "city": "The Hague", "longitude": "4.2700", "country": "Netherlands"}, "Jamaica": {"latitude": "17.9771", "city": "Kingston", "longitude": "-76.7674", "country": "Jamaica"}, "Trinidad & Tobago": {"latitude": "10.6520", "city": "Port-of-Spain", "longitude": "-61.5170", "country": "Trinidad & Tobago"}, "Oman": {"latitude": "23.6133", "city": "Muscat", "longitude": "58.5933", "country": "Oman"}, "Tanzania": {"latitude": "-6.8000", "city": "Dar es Salaam", "longitude": "39.2683", "country": "Tanzania"}, "Costa Rica": {"latitude": "9.9350", "city": "San José", "longitude": "-84.0841", "country": "Costa Rica"}, "Cabo Verde": {"latitude": "14.9167", "city": "Praia", "longitude": "-23.5167", "country": "Cabo Verde"}, "Christmas Island": {"latitude": "-10.4167", "city": "Flying Fish Cove", "longitude": "105.7167", "country": "Christmas Island"}, "Gabon": {"latitude": "0.3854", "city": "Libreville", "longitude": "9.4580", "country": "Gabon"}, "Niue": {"latitude": "-19.0167", "city": "Alofi", "longitude": "-169.9167", "country": "Niue"}, "Monaco": {"latitude": "43.7396", "city": "Monaco", "longitude": "7.4069", "country": "Monaco"}, "Samoa": {"latitude": "-13.8415", "city": "Apia", "longitude": "-171.7386", "country": "Samoa"}, "New Zealand": {"latitude": "-41.3000", "city": "Wellington", "longitude": "174.7833", "country": "New Zealand"}, "Saint Pierre & Miquelon": {"latitude": "46.7811", "city": "Saint-Pierre", "longitude": "-56.1764", "country": "Saint Pierre & Miquelon"}, "Jersey": {"latitude": "49.1833", "city": "Saint Helier", "longitude": "-2.1000", "country": "Jersey"}, "Pakistan": {"latitude": "33.7000", "city": "Islamabad", "longitude": "73.1666", "country": "Pakistan"}, "Albania": {"latitude": "41.3275", "city": "Tirana", "longitude": "19.8189", "country": "Albania"}, "West Bank": {"latitude": "31.7764", "city": "Al Quds", "longitude": "35.2269", "country": "West Bank"}, "Macau": {"latitude": "22.2030", "city": "Macau", "longitude": "113.5450", "country": "Macau"}, "Norfolk Island": {"latitude": "-29.0569", "city": "Kingston", "longitude": "167.9617", "country": "Norfolk Island"}, "United Arab Emirates": {"latitude": "24.4667", "city": "Abu Dhabi", "longitude": "54.3666", "country": "United Arab Emirates"}, "Guam": {"latitude": "13.4745", "city": "Hagåtña", "longitude": "144.7504", "country": "Guam"}, "Uruguay": {"latitude": "-34.8580", "city": "Montevideo", "longitude": "-56.1711", "country": "Uruguay"}, "India": {"latitude": "28.6000", "city": "New Delhi", "longitude": "77.2000", "country": "India"}, "Azerbaijan": {"latitude": "40.3953", "city": "Baku", "longitude": "49.8622", "country": "Azerbaijan"}, "Lesotho": {"latitude": "-29.3167", "city": "Maseru", "longitude": "27.4833", "country": "Lesotho"}, "Kenya": {"latitude": "-1.2833", "city": "Nairobi", "longitude": "36.8167", "country": "Kenya"}, "South Korea": {"latitude": "37.5663", "city": "Seoul", "longitude": "126.9997", "country": "South Korea"}, "Belarus": {"latitude": "53.9000", "city": "Minsk", "longitude": "27.5666", "country": "Belarus"}, "Tajikistan": {"latitude": "38.5600", "city": "Dushanbe", "longitude": "68.7739", "country": "Tajikistan"}, "Greenland": {"latitude": "64.1983", "city": "Nuuk", "longitude": "-51.7327", "country": "Greenland"}, "Turkey": {"latitude": "39.9272", "city": "Ankara", "longitude": "32.8644", "country": "Turkey"}, "Afghanistan": {"latitude": "34.5167", "city": "Kabul", "longitude": "69.1833", "country": "Afghanistan"}, "Bangladesh": {"latitude": "23.7231", "city": "Dhaka", "longitude": "90.4086", "country": "Bangladesh"}, "Eritrea": {"latitude": "15.3333", "city": "Asmara", "longitude": "38.9333", "country": "Eritrea"}, "Solomon Islands": {"latitude": "-9.4380", "city": "Honiara", "longitude": "159.9498", "country": "Solomon Islands"}, "Viet Nam": {"latitude": "21.0333", "city": "Hanoi", "longitude": "105.8500", "country": "Viet Nam"}, "Saint Lucia": {"latitude": "14.0020", "city": "Castries", "longitude": "-61.0000", "country": "Saint Lucia"}, "San Marino": {"latitude": "43.9172", "city": "San Marino", "longitude": "12.4667", "country": "San Marino"}, "French Polynesia": {"latitude": "-17.5334", "city": "Papeete", "longitude": "-149.5667", "country": "French Polynesia"}, "France": {"latitude": "48.8667", "city": "Paris", "longitude": "2.3333", "country": "France"}, "Rwanda": {"latitude": "-1.9536", "city": "Kigali", "longitude": "30.0605", "country": "Rwanda"}, "Slovakia": {"latitude": "48.1500", "city": "Bratislava", "longitude": "17.1170", "country": "Slovakia"}, "Somalia": {"latitude": "2.0667", "city": "Mogadishu", "longitude": "45.3667", "country": "Somalia"}, "Peru": {"latitude": "-12.0480", "city": "Lima", "longitude": "-77.0501", "country": "Peru"}, "Laos": {"latitude": "17.9667", "city": "Vientiane", "longitude": "102.6000", "country": "Laos"}, "Republic of Congo": {"latitude": "-4.2592", "city": "Brazzaville", "longitude": "15.2847", "country": "Republic of Congo"}, "Norway": {"latitude": "59.9167", "city": "Oslo", "longitude": "10.7500", "country": "Norway"}, "Malawi": {"latitude": "-13.9833", "city": "Lilongwe", "longitude": "33.7833", "country": "Malawi"}, "Cook Islands": {"latitude": "-21.2500", "city": "Avarua", "longitude": "-159.7500", "country": "Cook Islands"}, "Benin": {"latitude": "6.4000", "city": "Cotonou", "longitude": "2.5200", "country": "Benin"}, "Korea": {"latitude": "37.5663", "city": "Seoul", "longitude": "126.9997", "country": "Korea"}, "Singapore": {"latitude": "1.2930", "city": "Singapore", "longitude": "103.8558", "country": "Singapore"}, "Montenegro": {"latitude": "42.4660", "city": "Podgorica", "longitude": "19.2663", "country": "Montenegro"}, "Republic of the Congo": {"latitude": "-4.2592", "city": "Brazzaville", "longitude": "15.2847", "country": "Republic of the Congo"}, "Togo": {"latitude": "6.1319", "city": "Lomé", "longitude": "1.2228", "country": "Togo"}, "China": {"latitude": "39.9289", "city": "Beijing", "longitude": "116.3883", "country": "China"}, "Armenia": {"latitude": "40.1812", "city": "Yerevan", "longitude": "44.5136", "country": "Armenia"}, "Dominican Republic": {"latitude": "18.4701", "city": "Santo Domingo", "longitude": "-69.9001", "country": "Dominican Republic"}, "Ukraine": {"latitude": "50.4334", "city": "Kiev", "longitude": "30.5166", "country": "Ukraine"}, "Ghana": {"latitude": "5.5500", "city": "Accra", "longitude": "-0.2167", "country": "Ghana"}, "Tonga": {"latitude": "-21.1385", "city": "Nukualofa", "longitude": "-175.2206", "country": "Tonga"}, "Indonesia": {"latitude": "-6.1744", "city": "Jakarta", "longitude": "106.8294", "country": "Indonesia"}, "Libya": {"latitude": "32.8925", "city": "Tripoli", "longitude": "13.1800", "country": "Libya"}, "Finland": {"latitude": "60.1756", "city": "Helsinki", "longitude": "24.9341", "country": "Finland"}, "Central African Republic": {"latitude": "4.3666", "city": "Bangui", "longitude": "18.5583", "country": "Central African Republic"}, "Mauritius": {"latitude": "-20.1666", "city": "Port Louis", "longitude": "57.5000", "country": "Mauritius"}, "Liechtenstein": {"latitude": "47.1337", "city": "Vaduz", "longitude": "9.5167", "country": "Liechtenstein"}, "Vietnam": {"latitude": "21.0333", "city": "Hanoi", "longitude": "105.8500", "country": "Vietnam"}, "Mali": {"latitude": "12.6500", "city": "Bamako", "longitude": "-8.0000", "country": "Mali"}, "Iran": {"latitude": "35.6719", "city": "Tehran", "longitude": "51.4243", "country": "Iran"}, "Cambodia": {"latitude": "11.5500", "city": "Phnom Penh", "longitude": "104.9166", "country": "Cambodia"}, "Russia": {"latitude": "55.7522", "city": "Moscow", "longitude": "37.6155", "country": "Russia"}, "Bulgaria": {"latitude": "42.6833", "city": "Sofia", "longitude": "23.3167", "country": "Bulgaria"}, "United States": {"latitude": "44.967243", "city": "United States", "longitude": "-103.771556", "country": "United States"}, "Romania": {"latitude": "44.4334", "city": "Bucharest", "longitude": "26.0999", "country": "Romania"}, "Angola": {"latitude": "-8.8383", "city": "Luanda", "longitude": "13.2344", "country": "Angola"}, "Portugal": {"latitude": "38.7227", "city": "Lisbon", "longitude": "-9.1449", "country": "Portugal"}, "South Africa": {"latitude": "-33.9200", "city": "Cape Town", "longitude": "18.4350", "country": "South Africa"}, "Cyprus": {"latitude": "35.1667", "city": "Nicosia", "longitude": "33.3666", "country": "Cyprus"}, "Sweden": {"latitude": "59.3508", "city": "Stockholm", "longitude": "18.0973", "country": "Sweden"}, "Antigua & Barbuda": {"latitude": "17.1180", "city": "Saint John's", "longitude": "-61.8500", "country": "Antigua & Barbuda"}, "Malaysia": {"latitude": "3.1667", "city": "Kuala Lumpur", "longitude": "101.7000", "country": "Malaysia"}, "Senegal": {"latitude": "14.7158", "city": "Dakar", "longitude": "-17.4731", "country": "Senegal"}, "Mozambique": {"latitude": "-25.9553", "city": "Maputo", "longitude": "32.5892", "country": "Mozambique"}, "Uganda": {"latitude": "0.3167", "city": "Kampala", "longitude": "32.5833", "country": "Uganda"}, "Hungary": {"latitude": "47.5000", "city": "Budapest", "longitude": "19.0833", "country": "Hungary"}, "Niger": {"latitude": "13.5167", "city": "Niamey", "longitude": "2.1167", "country": "Niger"}, "Saint Martin": {"latitude": "18.0706", "city": "Marigot", "longitude": "-63.0847", "country": "Saint Martin"}, "Faroe Islands": {"latitude": "62.0300", "city": "Tórshavn", "longitude": "-6.8200", "country": "Faroe Islands"}, "Guinea": {"latitude": "9.5315", "city": "Conakry", "longitude": "-13.6802", "country": "Guinea"}, "Panama": {"latitude": "8.9680", "city": "Panama City", "longitude": "-79.5330", "country": "Panama"}, "Guyana": {"latitude": "6.8020", "city": "Georgetown", "longitude": "-58.1670", "country": "Guyana"}, "Qatar": {"latitude": "25.2866", "city": "Doha", "longitude": "51.5330", "country": "Qatar"}, "Luxembourg": {"latitude": "49.6117", "city": "Luxembourg", "longitude": "6.1300", "country": "Luxembourg"}, "Andorra": {"latitude": "42.5000", "city": "Andorra", "longitude": "1.5165", "country": "Andorra"}, "Gibraltar": {"latitude": "36.1324", "city": "Gibraltar", "longitude": "-5.3781", "country": "Gibraltar"}, "Ireland": {"latitude": "53.3331", "city": "Dublin", "longitude": "-6.2489", "country": "Ireland"}, "Italy": {"latitude": "41.8960", "city": "Rome", "longitude": "12.4833", "country": "Italy"}, "Nigeria": {"latitude": "9.0833", "city": "Abuja", "longitude": "7.5333", "country": "Nigeria"}, "Ecuador": {"latitude": "-0.2150", "city": "Quito", "longitude": "-78.5001", "country": "Ecuador"}, "Czech Republic": {"latitude": "50.0833", "city": "Prague", "longitude": "14.4660", "country": "Czech Republic"}, "Brunei": {"latitude": "4.8833", "city": "Bandar Seri Begawan", "longitude": "114.9333", "country": "Brunei"}, "Australia": {"latitude": "-35.2830", "city": "Canberra", "longitude": "149.1290", "country": "Australia"}, "Vanuatu": {"latitude": "-17.7334", "city": "Port Vila", "longitude": "168.3166", "country": "Vanuatu"}, "Saint Kitts & Nevis": {"latitude": "17.3020", "city": "Basseterre", "longitude": "-62.7170", "country": "Saint Kitts & Nevis"}, "Algeria": {"latitude": "36.7631", "city": "Algiers", "longitude": "3.0506", "country": "Algeria"}, "Svalbard": {"latitude": "78.2167", "city": "Longyearbyen", "longitude": "15.6333", "country": "Svalbard"}, "El Salvador": {"latitude": "13.7100", "city": "San Salvador", "longitude": "-89.2030", "country": "El Salvador"}, "Tuvalu": {"latitude": "-8.5167", "city": "Funafuti", "longitude": "179.2166", "country": "Tuvalu"}, "Czechia": {"latitude": "50.0833", "city": "Prague", "longitude": "14.4660", "country": "Czechia"}, "Pitcairn Islands": {"latitude": "-25.0667", "city": "Adamstown", "longitude": "-130.0833", "country": "Pitcairn Islands"}, "Turks & Caicos Islands": {"latitude": "21.4664", "city": "Grand Turk", "longitude": "-71.1360", "country": "Turks & Caicos Islands"}, "Marshall Islands": {"latitude": "7.1030", "city": "Majuro", "longitude": "171.3800", "country": "Marshall Islands"}, "Chile": {"latitude": "-33.4500", "city": "Santiago", "longitude": "-70.6670", "country": "Chile"}, "Belgium": {"latitude": "50.8333", "city": "Brussels", "longitude": "4.3333", "country": "Belgium"}, "Thailand": {"latitude": "13.7500", "city": "Bangkok", "longitude": "100.5166", "country": "Thailand"}, "Haiti": {"latitude": "18.5410", "city": "Port-au-Prince", "longitude": "-72.3360", "country": "Haiti"}, "Belize": {"latitude": "17.2520", "city": "Belmopan", "longitude": "-88.7671", "country": "Belize"}, "Hong Kong": {"latitude": "22.3050", "city": "Hong Kong", "longitude": "114.1850", "country": "Hong Kong"}, "Sierra Leone": {"latitude": "8.4700", "city": "Freetown", "longitude": "-13.2342", "country": "Sierra Leone"}, "Georgia": {"latitude": "41.7250", "city": "Tbilisi", "longitude": "44.7908", "country": "Georgia"}, "Denmark": {"latitude": "55.6786", "city": "København", "longitude": "12.5635", "country": "Denmark"}, "Poland": {"latitude": "52.2500", "city": "Warsaw", "longitude": "21.0000", "country": "Poland"}, "Moldova": {"latitude": "47.0050", "city": "Chișinău", "longitude": "28.8577", "country": "Moldova"}, "French Guiana": {"latitude": "4.9330", "city": "Cayenne", "longitude": "-52.3300", "country": "French Guiana"}, "Morocco": {"latitude": "34.0253", "city": "Rabat", "longitude": "-6.8361", "country": "Morocco"}, "Namibia": {"latitude": "-22.5700", "city": "Windhoek", "longitude": "17.0835", "country": "Namibia"}, "Mongolia": {"latitude": "47.9167", "city": "Ulaanbaatar", "longitude": "106.9166", "country": "Mongolia"}, "Guernsey": {"latitude": "49.4561", "city": "Saint Peter Port", "longitude": "-2.5408", "country": "Guernsey"}, "Kiribati": {"latitude": "1.3382", "city": "Tarawa", "longitude": "173.0176", "country": "Kiribati"}, "Switzerland": {"latitude": "46.9167", "city": "Bern", "longitude": "7.4670", "country": "Switzerland"}, "Grenada": {"latitude": "12.0526", "city": "Saint George's", "longitude": "-61.7416", "country": "Grenada"}, "Seychelles": {"latitude": "-4.6166", "city": "Victoria", "longitude": "55.4500", "country": "Seychelles"}, "Chad": {"latitude": "12.1131", "city": "Ndjamena", "longitude": "15.0491", "country": "Chad"}, "Estonia": {"latitude": "59.4339", "city": "Tallinn", "longitude": "24.7280", "country": "Estonia"}, "Kosovo": {"latitude": "42.6666", "city": "Pristina", "longitude": "21.1724", "country": "Kosovo"}, "Equatorial Guinea": {"latitude": "3.7500", "city": "Malabo", "longitude": "8.7833", "country": "Equatorial Guinea"}, "Lebanon": {"latitude": "33.8720", "city": "Beirut", "longitude": "35.5097", "country": "Lebanon"}, "Uzbekistan": {"latitude": "41.3117", "city": "Tashkent", "longitude": "69.2949", "country": "Uzbekistan"}, "Egypt": {"latitude": "30.0500", "city": "Cairo", "longitude": "31.2500", "country": "Egypt"}, "Djibouti": {"latitude": "11.5950", "city": "Djibouti", "longitude": "43.1480", "country": "Djibouti"}, "Bermuda": {"latitude": "32.2942", "city": "Hamilton", "longitude": "-64.7839", "country": "Bermuda"}, "Timor-Leste": {"latitude": "-8.5594", "city": "Dili", "longitude": "125.5795", "country": "Timor-Leste"}, "Spain": {"latitude": "40.4000", "city": "Madrid", "longitude": "-3.6834", "country": "Spain"}, "Colombia": {"latitude": "4.5964", "city": "Bogota", "longitude": "-74.0833", "country": "Colombia"}, "Burundi": {"latitude": "-3.3761", "city": "Bujumbura", "longitude": "29.3600", "country": "Burundi"}, "Taiwan": {"latitude": "25.0358", "city": "Taipei", "longitude": "121.5683", "country": "Taiwan"}, "Nicaragua": {"latitude": "12.1530", "city": "Managua", "longitude": "-86.2685", "country": "Nicaragua"}, "Barbados": {"latitude": "13.1020", "city": "Bridgetown", "longitude": "-59.6165", "country": "Barbados"}, "Falkland Islands (Islas Malvinas)": {"latitude": "-51.7000", "city": "Stanley", "longitude": "-57.8500", "country": "Falkland Islands (Islas Malvinas)"}, "Madagascar": {"latitude": "-18.9166", "city": "Antananarivo", "longitude": "47.5166", "country": "Madagascar"}, "Isle Of Man": {"latitude": "54.1504", "city": "Douglas", "longitude": "-4.4800", "country": "Isle Of Man"}, "Palau": {"latitude": "7.5000", "city": "Ngerulmud", "longitude": "134.6242", "country": "Palau"}, "Bhutan": {"latitude": "27.4730", "city": "Thimphu", "longitude": "89.6390", "country": "Bhutan"}, "Sudan": {"latitude": "15.5881", "city": "Khartoum", "longitude": "32.5342", "country": "Sudan"}, "Nepal": {"latitude": "27.7167", "city": "Kathmandu", "longitude": "85.3166", "country": "Nepal"}, "Malta": {"latitude": "35.8997", "city": "Valletta", "longitude": "14.5147", "country": "Malta"}, "Democratic Republic of the Congo": {"latitude": "-4.3297", "city": "Kinshasa", "longitude": "15.3150", "country": "Democratic Republic of the Congo"}, "Maldives": {"latitude": "4.1667", "city": "Malé", "longitude": "73.4999", "country": "Maldives"}, "Suriname": {"latitude": "5.8350", "city": "Paramaribo", "longitude": "-55.1670", "country": "Suriname"}, "Cayman Islands": {"latitude": "19.2804", "city": "George Town", "longitude": "-81.3300", "country": "Cayman Islands"}, "Anguilla": {"latitude": "18.2167", "city": "The Valley", "longitude": "-63.0500", "country": "Anguilla"}, "Venezuela": {"latitude": "10.5010", "city": "Caracas", "longitude": "-66.9170", "country": "Venezuela"}, "Israel": {"latitude": "31.7784", "city": "Jerusalem", "longitude": "35.2066", "country": "Israel"}, "Iceland": {"latitude": "64.1500", "city": "Reykjavík", "longitude": "-21.9500", "country": "Iceland"}, "Zambia": {"latitude": "-15.4166", "city": "Lusaka", "longitude": "28.2833", "country": "Zambia"}, "Austria": {"latitude": "48.2000", "city": "Vienna", "longitude": "16.3666", "country": "Austria"}, "Papua New Guinea": {"latitude": "-9.4647", "city": "Port Moresby", "longitude": "147.1925", "country": "Papua New Guinea"}, "Cote d'Ivoire": {"latitude": "6.8184", "city": "Yamoussoukro", "longitude": "-5.2755", "country": "Cote d'Ivoire"}, "Sao Tome & Principe": {"latitude": "0.3334", "city": "São Tomé", "longitude": "6.7333", "country": "Sao Tome & Principe"}, "Germany": {"latitude": "52.5218", "city": "Berlin", "longitude": "13.4015", "country": "Germany"}, "Gambia": {"latitude": "13.4539", "city": "Banjul", "longitude": "-16.5917", "country": "Gambia"}, "Kazakhstan": {"latitude": "51.1811", "city": "Astana", "longitude": "71.4278", "country": "Kazakhstan"}, "Philippines": {"latitude": "14.6042", "city": "Manila", "longitude": "120.9822", "country": "Philippines"}, "Mauritania": {"latitude": "18.0864", "city": "Nouakchott", "longitude": "-15.9753", "country": "Mauritania"}, "Kyrgyzstan": {"latitude": "42.8731", "city": "Bishkek", "longitude": "74.5852", "country": "Kyrgyzstan"}, "Iraq": {"latitude": "33.3386", "city": "Baghdad", "longitude": "44.3939", "country": "Iraq"}, "Montserrat": {"latitude": "16.7000", "city": "Plymouth", "longitude": "-62.2167", "country": "Montserrat"}, "New Caledonia": {"latitude": "-22.2625", "city": "Nouméa", "longitude": "166.4443", "country": "New Caledonia"}, "Macedonia": {"latitude": "42.0000", "city": "Skopje", "longitude": "21.4335", "country": "Macedonia"}, "North Korea": {"latitude": "39.0194", "city": "Pyongyang", "longitude": "125.7547", "country": "North Korea"}, "Sri Lanka": {"latitude": "6.9000", "city": "Sri Jawewardenepura Kotte", "longitude": "79.9500", "country": "Sri Lanka"}, "Latvia": {"latitude": "56.9500", "city": "Riga", "longitude": "24.1000", "country": "Latvia"}, "South Sudan": {"latitude": "4.8300", "city": "Juba", "longitude": "31.5800", "country": "South Sudan"}, "Curaçao": {"latitude": "12.2004", "city": "Willemstad", "longitude": "-69.0200", "country": "Curaçao"}, "Croatia": {"latitude": "45.8000", "city": "Zagreb", "longitude": "16.0000", "country": "Croatia"}, "Syria": {"latitude": "33.5000", "city": "Damascus", "longitude": "36.3000", "country": "Syria"}, "Guadeloupe": {"latitude": "15.99854", "city": "Basse-Terre", "longitude": "-61.72548", "country": "Guadeloupe"}, "Sint Maarten": {"latitude": "18.0255", "city": "Philipsburg", "longitude": "-63.0450", "country": "Sint Maarten"}, "Burma": {"latitude": "19.7666", "city": "Naypyidaw", "longitude": "96.1186", "country": "Burma"}, "Honduras": {"latitude": "14.1020", "city": "Tegucigalpa", "longitude": "-87.2175", "country": "Honduras"}, "Myanmar": {"latitude": "16.7834", "city": "Rangoon", "longitude": "96.1667", "country": "Myanmar"}, "Mexico": {"latitude": "19.4424", "city": "Mexico City", "longitude": "-99.1310", "country": "Mexico"}, "Tunisia": {"latitude": "36.8028", "city": "Tunis", "longitude": "10.1797", "country": "Tunisia"}, "Cuba": {"latitude": "23.1320", "city": "Havana", "longitude": "-82.3642", "country": "Cuba"}, "Serbia": {"latitude": "44.8186", "city": "Belgrade", "longitude": "20.4680", "country": "Serbia"}, "Comoros": {"latitude": "-11.7042", "city": "Moroni", "longitude": "43.2402", "country": "Comoros"}, "United Kingdom": {"latitude": "51.5000", "city": "London", "longitude": "-0.1167", "country": "United Kingdom"}, "Congo": {"latitude": "-4.2592", "city": "Brazzaville", "longitude": "15.2847", "country": "Congo"}, "Greece": {"latitude": "37.9833", "city": "Athens", "longitude": "23.7333", "country": "Greece"}, "Paraguay": {"latitude": "-25.2964", "city": "Asunción", "longitude": "-57.6415", "country": "Paraguay"}, "Saint Vincent & The Grenadines": {"latitude": "13.1483", "city": "Kingstown", "longitude": "-61.2121", "country": "Saint Vincent & The Grenadines"}, "Botswana": {"latitude": "-24.6463", "city": "Gaborone", "longitude": "25.9119", "country": "Botswana"}}

state_hash =  {
  "AL": {
    "name": "Alabama",
    "capital": "Montgomery",
    "lat": "32.361538",
    "long": "-86.279118"
  },
  "AK": {
    "name": "Alaska",
    "capital": "Juneau",
    "lat": "58.301935",
    "long": "-134.419740"
  },
  "AZ": {
    "name": "Arizona",
    "capital": "Phoenix",
    "lat": "33.448457",
    "long": "-112.073844"
  },
  "AR": {
    "name": "Arkansas",
    "capital": "Little Rock",
    "lat": "34.736009",
    "long": "-92.331122"
  },
  "CA": {
    "name": "California",
    "capital": "Sacramento",
    "lat": "38.555605",
    "long": "-121.468926"
  },
  "CO": {
    "name": "Colorado",
    "capital": "Denver",
    "lat": "39.7391667",
    "long": "-104.984167"
  },
  "CT": {
    "name": "Connecticut",
    "capital": "Hartford",
    "lat": "41.767",
    "long": "-72.677"
  },
  "DE": {
    "name": "Delaware",
    "capital": "Dover",
    "lat": "39.161921",
    "long": "-75.526755"
  },
  "FL": {
    "name": "Florida",
    "capital": "Tallahassee",
    "lat": "30.4518",
    "long": "-84.27277"
  },
  "GA": {
    "name": "Georgia",
    "capital": "Atlanta",
    "lat": "33.76",
    "long": "-84.39"
  },
  "GU": {
    "name": "Guam",
    "capital": "Hagåtña",
    "lat": "13.4745",
    "long": "144.7504",
  },
  "HI": {
    "name": "Hawaii",
    "capital": "Honolulu",
    "lat": "21.30895",
    "long": "-157.826182"
  },
  "ID": {
    "name": "Idaho",
    "capital": "Boise",
    "lat": "43.613739",
    "long": "-116.237651"
  },
  "IL": {
    "name": "Illinois",
    "capital": "Springfield",
    "lat": "39.783250",
    "long": "-89.650373"
  },
  "IN": {
    "name": "Indiana",
    "capital": "Indianapolis",
    "lat": "39.790942",
    "long": "-86.147685"
  },
  "IA": {
    "name": "Iowa",
    "capital": "Des Moines",
    "lat": "41.590939",
    "long": "-93.620866"
  },
  "KS": {
    "name": "Kansas",
    "capital": "Topeka",
    "lat": "39.04",
    "long": "-95.69"
  },
  "KY": {
    "name": "Kentucky",
    "capital": "Frankfort",
    "lat": "38.197274",
    "long": "-84.86311"
  },
  "LA": {
    "name": "Louisiana",
    "capital": "Baton Rouge",
    "lat": "30.45809",
    "long": "-91.140229"
  },
  "ME": {
    "name": "Maine",
    "capital": "Augusta",
    "lat": "44.323535",
    "long": "-69.765261"
  },
  "MD": {
    "name": "Maryland",
    "capital": "Annapolis",
    "lat": "38.972945",
    "long": "-76.501157"
  },
  "MA": {
    "name": "Massachusetts",
    "capital": "Boston",
    "lat": "42.2352",
    "long": "-71.0275"
  },
  "MI": {
    "name": "Michigan",
    "capital": "Lansing",
    "lat": "42.7335",
    "long": "-84.5467"
  },
  "MN": {
    "name": "Minnesota",
    "capital": "Saint Paul",
    "lat": "44.95",
    "long": "-93.094"
  },
  "MS": {
    "name": "Mississippi",
    "capital": "Jackson",
    "lat": "32.320",
    "long": "-90.207"
  },
  "MO": {
    "name": "Missouri",
    "capital": "Jefferson City",
    "lat": "38.572954",
    "long": "-92.189283"
  },
  "MT": {
    "name": "Montana",
    "capital": "Helana",
    "lat": "46.595805",
    "long": "-112.027031"
  },
  "NE": {
    "name": "Nebraska",
    "capital": "Lincoln",
    "lat": "40.809868",
    "long": "-96.675345"
  },
  "NV": {
    "name": "Nevada",
    "capital": "Carson City",
    "lat": "39.160949",
    "long": "-119.753877"
  },
  "NH": {
    "name": "New Hampshire",
    "capital": "Concord",
    "lat": "43.220093",
    "long": "-71.549127"
  },
  "NJ": {
    "name": "New Jersey",
    "capital": "Trenton",
    "lat": "40.221741",
    "long": "-74.756138"
  },
  "NM": {
    "name": "New Mexico",
    "capital": "Santa Fe",
    "lat": "35.667231",
    "long": "-105.964575"
  },
  "NY": {
    "name": "New York",
    "capital": "Albany",
    "lat": "42.659829",
    "long": "-73.781339"
  },
  "NC": {
    "name": "North Carolina",
    "capital": "Raleigh",
    "lat": "35.771",
    "long": "-78.638"
  },
  "ND": {
    "name": "North Dakota",
    "capital": "Bismarck",
    "lat": "48.813343",
    "long": "-100.779004"
  },
  "OH": {
    "name": "Ohio",
    "capital": "Columbus",
    "lat": "39.962245",
    "long": "-83.000647"
  },
  "OK": {
    "name": "Oklahoma",
    "capital": "Oklahoma City",
    "lat": "35.482309",
    "long": "-97.534994"
  },
  "OR": {
    "name": "Oregon",
    "capital": "Salem",
    "lat": "44.931109",
    "long": "-123.029159"
  },
  "PA": {
    "name": "Pennsylvania",
    "capital": "Harrisburg",
    "lat": "40.269789",
    "long": "-76.875613"
  },
  "PR": {
    "name": "Puerto Rico",
    "capital": "San Juan",
    "lat": "18.45",
    "long": "-66.1"
  },
    "RI": {
    "name": "Rhode Island",
    "capital": "Providence",
    "lat": "41.82355",
    "long": "-71.422132"
  },
  "SC": {
    "name": "South Carolina",
    "capital": "Columbia",
    "lat": "34.000",
    "long": "-81.035"
  },
  "SD": {
    "name": "South Dakota",
    "capital": "Pierre",
    "lat": "44.367966",
    "long": "-100.336378"
  },
  "TN": {
    "name": "Tennessee",
    "capital": "Nashville",
    "lat": "36.165",
    "long": "-86.784"
  },
  "TX": {
    "name": "Texas",
    "capital": "Austin",
    "lat": "30.266667",
    "long": "-97.75"
  },
  "UT": {
    "name": "Utah",
    "capital": "Salt Lake City",
    "lat": "40.7547",
    "long": "-111.892622"
  },
  "VT": {
    "name": "Vermont",
    "capital": "Montpelier",
    "lat": "44.26639",
    "long": "-72.57194"
  },
  "VA": {
    "name": "Virginia",
    "capital": "Richmond",
    "lat": "37.54",
    "long": "-77.46"
  },
  "WA": {
    "name": "Washington",
    "capital": "Olympia",
    "lat": "47.042418",
    "long": "-122.893077"
  },
  "WV": {
    "name": "West Virginia",
    "capital": "Charleston",
    "lat": "38.349497",
    "long": "-81.633294"
  },
  "WI": {
    "name": "Wisconsin",
    "capital": "Madison",
    "lat": "43.074722",
    "long": "-89.384444"
  },
  "WY": {
    "name": "Wyoming",
    "capital": "Cheyenne",
    "lat": "41.145548",
    "long": "-104.802042"
  }
}
