#!/usr/bin/env python3

import os
import sys
import time
import subprocess
import shlex
from send_email import send_email

def exiting(message, notify=False):
    print(message, file=sys.stderr)
    print("FAIL", file=sys.stderr)
    if notify and os.environ.get("EMAIL_RECIPIENT") is not None:
        long_message = "{}\n{}".format(" ".join(sys.argv), message)
        send_email(os.environ.get("EMAIL_RECIPIENT"), 'EBI viral pipeline error', long_message)
    sys.exit(1)

def stopping(message):
    print(message, file=sys.stderr)
    print("DONE", file=sys.stderr)
    sys.exit(2)

def timing(t0, message):
    t1 = time.time()
    print("{0} Time used: {1} seconds".format(message, int(t1-t0)), file=sys.stdout)
    return

def call_cmd(cmd):
    p = subprocess.run(shlex.split(cmd))
    return p.returncode

def call_shell(cmd):
    p = subprocess.run(cmd, shell=True)
    return p.returncode

def call_silent(command):
    try:
        devnull = open(os.devnull, 'w')
    except IOError as e:
        exiting("Devnull won't open.")
    p = subprocess.run(shlex.split(command), stdout=devnull, stderr=devnull)
    devnull.close()
    return p.returncode
