#!/usr/bin/env python3

from geo_hash import countries_hash, state_hash

def interpret_date(val):
    '''
    Adapted from NCBI-Downloader from Martin C. F. Thomsen
    This function will try to interpret the
    :param: val Date
    :return: List of integers: [yyyy, mm, dd] or ["", "", ""] if
        value not identified
        >>> InterpretDate('Feb-30-2014')
        [2014, 2, 30]
        >>> InterpretDate('Feb-2014')
        ['2014', 2, ""]
    '''
    month = 'Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec'.split()
    yyyy, mm, dd = None, None, None
    if '/' in val:
        # American style
        try:
            mm, dd, yyyy = val.split('/')
        except:
            pass
    elif '-' in val:
        # European style
        tmp = val.split('-')
        lens = [len(x) for x in tmp]
        lenl = len(lens)
        if lenl == 3:
            if lens[2] == 4:
                yyyy = tmp[2]
                if tmp[1] in month and lens[0] <= 2:
                    mm = month.index(tmp[1]) + 1
                    dd = tmp[0]
                elif tmp[0] in month and lens[1] <= 2:
                    mm = month.index(tmp[0]) + 1
                    dd = tmp[1]
                elif lens[0] <= 2 and lens[1] <= 2:
                    mm = tmp[1]
                    dd = tmp[0]
            elif lens[0] == 4:
                yyyy = tmp[0]
                if tmp[1] in month and lens[2] <= 2:
                    mm = month.index(tmp[1]) + 1
                    dd = tmp[2]
                elif lens[1] <= 2 and lens[2] <= 2:
                    mm = tmp[1]
                    dd = tmp[2]
            elif lens[0] <= 2 and lens[2] == 2 and tmp[1] in month:
                yearnow = datetime.now().year
                yyyy = int(str(yearnow)[:-2] + tmp[2])
                if yyyy > yearnow:
                    yyyy = int(str(yearnow-100)[:-2] + tmp[2])
                mm = month.index(tmp[1]) + 1
                dd = tmp[0]
        elif lenl == 2:
            if lens[0] == 4:
                yyyy = tmp[0]
                if tmp[1] in month:
                    mm = month.index(tmp[1]) + 1
                elif lens[1] <= 2:
                    mm = tmp[1]
            if lens[1] == 4:
                yyyy = tmp[1]
                if tmp[0] in month:
                    mm = month.index(tmp[0]) + 1
                elif lens[0] <= 2:
                    mm = tmp[0]
            elif lens[1] == 2 and tmp[0] in month:
                yyyy, mm = '20' + tmp[1], month.index(tmp[0]) + 1
            elif lens[0] == 2 and tmp[1] in month:
                yyyy, mm = '20' + tmp[0], month.index(tmp[1]) + 1
    elif len(val) == 4:
        yyyy = val
    return [str(yyyy) if yyyy is not None else "",
            str(mm) if mm is not None else "",
            str(dd) if dd is not None else ""]

def interpret_loc(val):
    '''  '''
    geo_dict = {
        'longitude': 'NA',
        'latitude': 'NA',
        'country' : val,
        'region': ''}

    countries = list(countries_hash.keys())
    if val is None or val in ["NA", 'Missing', "Not applicable", "Unknown"]:
        return geo_dict

    elif val in countries or val.split(":")[0].strip() in countries:
        tmp = val.split(":")
        country = tmp[0].strip()
        if len(tmp) > 1:
            geo_dict['country'] = country
            geo_dict['region'] = tmp[1].strip().split(",")[0].strip()
        country_info = countries_hash.get(country)
        if country_info is not None:
            geo_dict['longitude'] = country_info['longitude']
            geo_dict['latitude'] = country_info['latitude']
            return geo_dict

    val = val.upper()
    # check for usa
    if val.startswith("USA:"):
        geo_dict['country'] = "United States"
        # USA: Tacna, AZ
        rest = val.split(":", 1)[-1].strip()
        loc_parts = rest.split(",")
        for part in loc_parts:
            part = part.strip()
            if len(part) == 2:
                state_info = state_hash.get(part)
                if state_info is not None:
                    geo_dict['longitude'] = state_info['long']
                    geo_dict['latitude'] = state_info['lat']
                    geo_dict['region'] = state_info['name']
                    return geo_dict

            else:
                # USA: Texas
                for state in state_hash.values():
                    if part == state['name'].upper():
                        geo_dict['longitude'] = state['long']
                        geo_dict['latitude'] = state['lat']
                        geo_dict['region'] = state['name']
                        return geo_dict

        # Add unknown isolates to center
        if geo_dict['longitude'] == 'NA':
            geo_dict['longitude'] = '-103.771556'
            geo_dict['latitude'] = '44.967243'

    return geo_dict
