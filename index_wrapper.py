#!/usr/bin/env python3

import sys
import os
import shutil
import shlex
import subprocess
import vu_service

if (len(sys.argv) < 3):
    print("program <multi fasta or pattern> <output prefix> <k> <Sparse prefix> <HR threshold>")
    sys.exit(1)

## Check the PATH
if shutil.which("kma_index") is None:
    vu_service.exiting("KMA not in path")

index_cmd = "kma_index -i {0} -o {1} -k {2} -NI".format(sys.argv[1], sys.argv[2], sys.argv[3])
sparse_cmd = ""
if (len(sys.argv) > 4):
    if not sys.argv[4].isspace():
        sparse_cmd = " -Sparse {}".format(sys.argv[4])
    if (len(sys.argv) > 5):
        try:
            hr_thr = float(sys.argv[5])
            if hr_thr > 1.0:
                hr_thr = hr_thr / 100
            sparse_cmd += " -ht {0:.6f} -hq {0:.6f} -and".format(hr_thr)
        except ValueError:
            pass

index_cmd += sparse_cmd
with open("{}.log".format(sys.argv[2]), "w") as ofile:
    p = subprocess.run(shlex.split(index_cmd), stdout=ofile, stderr=subprocess.STDOUT)
