#!/usr/bin/sh

if [ $# -eq 0 ]
then
  echo "<program> <reference fasta> <info to rename>"
else
  cp $1 ${1}.original
  while read p; do N=$(echo ${p} | cut -d " " -f 1); sed -i "s|>$N.*|>$p|" $1; done < $2
  sed -i 's|	| |' $1 
fi
