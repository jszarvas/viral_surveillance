#!/usr/bin/env python3

import sys, os, time
import argparse
import tempfile
import pickle
import shutil
import multiprocessing
import sqlite3
import vu_service
import re
import gzip
import configparser

J_LIMIT = 12

def loc_arg_check(param):
    if param is not None and (os.path.exists(param) or os.path.exists("{}.name".format(param))):
        return
    else:
        vu_service.exiting("Error: {}".format(str(param)))

def batch_trashfiles(filelist, batch_size = 500):
    batch_cmd = []
    for i in range(0,len(trash_files),batch_size):
        batch_cmd.append("rm {}".format(" ".join(trash_files[i:i+batch_size])))
    return batch_cmd

def open_(filename, mode=None, compresslevel=9):
   """Switch for both open() and gzip.open().

   Determines if the file is normal or gzipped by looking at the file
   extension.

   The filename argument is required; mode defaults to 'rb' for gzip and 'r'
   for normal and compresslevel defaults to 9 for gzip.

   """
   if filename[-3:] == '.gz':
      if mode is None: mode = 'rb'
      return gzip.open(filename, mode, compresslevel)
   else:
      if mode is None: mode = 'r'
      return open(filename, mode)

def type_test(filename):
    if filename[-3:] == '.gz':
        gzip_obj = gzip.GzipFile(filename, 'rb', compresslevel=9)
        chunk = gzip_obj.peek(1000)
        lines = chunk.split(b"\n")
        if lines[0].startswith(b">"):
            return "fasta"
        read_lens = []
        for i in range(1,len(lines), 4):
            read_lens.append(len(lines[i]))
        if not read_lens:
            return ""
        avr_rlen = sum(read_lens)/len(read_lens)
        if avr_rlen > 300:
            return "nanopore"
        else:
            return "short"
    else:
        with open(filename, "r") as fp:
            line = fp.readline()
            if line[0] == ">":
                return "fasta"
            elif line[0] == "@":
                fp.seek(0)
                chunk = fp.read(10000)
                lines = chunk.split("\n")
                read_lens = []
                for i in range(1,len(lines), 4):
                    read_lens.append(len(lines[i]))
                if not read_lens:
                    return ""
                avr_rlen = sum(read_lens)/len(read_lens)
                if avr_rlen > 300:
                    return "nanopore"
                else:
                    return "short"

def get_template(filename, threshold):
    try:
        highest_coverage = 0.0
        tmp = (os.path.basename(filename[:-4]), None, None, 0)
        templates = []
        aln_templates = []
        with open(filename, "r") as f:
            # Skip header
            _ = f.readline()
            for l in f:
                #_________0	__1	_____2	_________3	______________4	_____________5	________________6	____7	_________________8	____________________9	_______10	_____11	_____12
                #Template	Num	Score	Expected	Template_length	Query_Coverage	Template_Coverage	Depth	tot_query_Coverage	tot_template_Coverage	tot_depth	q_value	p_value
                #MF541374:Rodent_Torque_teno_virus_3	11451	30985	0	6836	   90.60	  100.00	    4.53	   90.60	  100.00    4.53	30984.98	1.0e-26
                d = [x.strip() for x in l.split('\t')]
                if len(d) > 12:
                    template_coverage = float(d[9])
                    q_coverage = float(d[8])
                    templ = re.sub(r"\W+","_", d[0])
                    if (template_coverage > threshold):
                        if templates and not args.sim:
                            # how far is it from the threshold
                            if template_coverage < ( 0.5 * threshold + 0.5 * templates[0][2] ):
                                continue
                        templates.append((os.path.basename(filename[:-4]), templ, template_coverage, 1))
                        aln_templates.append([os.path.basename(filename[:-4]), templ, d[1]])
                    elif (template_coverage > highest_coverage):
                        highest_coverage = template_coverage
                        tmp = (os.path.basename(filename[:-4]), templ, template_coverage, 0)
        if not len(templates) > 0:
            templates.append(tmp)
        return templates, aln_templates
    except IOError as e:
        sys.stderr.write('KMA results could not be found for %s!\n%s'%(filename, e))
        return [tmp], None

parser = argparse.ArgumentParser(
    description='Run KMA Sparse and parse results')
parser.add_argument(
    '-d',
    dest="database",
    default=None,
    help='Sample db')
parser.add_argument(
    '-o',
    dest="odir",
    default=None,
    help='Output folder for tsv')
parser.add_argument(
    '-r',
    dest="kma_db",
    default=None,
    help="KMA database")
parser.add_argument(
    '-t',
    dest="id",
    default=72.00,
    type=float,
    help='Template id threshold, kmerspace, percentage')
parser.add_argument(
    '-s',
    dest="sim",
    action="store_true",
    help="Simulate reads for KMA alignment")
args = parser.parse_args()


##MAIN##
t0 = time.time()

## Check the PATH
if shutil.which("kma") is None:
    vu_service.exiting("KMA not in path")
if shutil.which("simreads_multi.py") is None:
    vu_service.exiting("Read simulator not in path")

# Check existence of parameters
loc_arg_check(args.database)
loc_arg_check(args.kma_db)
loc_arg_check(args.odir)

# Create tempfolder for simreads
tmp_folder = tempfile.mkdtemp(prefix='kma_folder_', dir=args.odir)

# get confidence parameter from config
config_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config.ini')
if os.path.exists(os.path.join(os.path.realpath(os.path.join(args.odir)), 'config.ini')):
    config_file = os.path.join(os.path.realpath(os.path.join(args.odir)), 'config.ini')
config = configparser.ConfigParser()
config.read(config_file)

# open database
conn = sqlite3.connect(args.database)
conn.execute("PRAGMA foreign_keys = 1")
cur = conn.cursor()

cur.execute('''CREATE TABLE IF NOT EXISTS templates
    (db_id INTEGER PRIMARY KEY,
    accession TEXT,
    template TEXT,
    templ_cov REAL,
    qc_pass INT DEFAULT 1,
    FOREIGN KEY(accession) REFERENCES samples(accession) ON DELETE CASCADE
)''')

cur.execute('''CREATE TABLE IF NOT EXISTS sequences
    (db_id INTEGER PRIMARY KEY,
    repr_id TEXT DEFAULT 'N',
    FOREIGN KEY(db_id) REFERENCES templates(db_id)
)''')

conn.commit()

sim_cmds = []
kma_cmds = []
spa_files = []
path_dict = {}
type_dict = {}
accessions = []
not_found_update = []
# Get all non-processed samples
cur.execute('''SELECT accession,path from samples where included is Null;''')
rows = cur.fetchall()
if rows is not None:
    for r in rows:
        # fastq or fasta
        paths = r[1].split(",")
        if not os.path.exists(paths[0]):
            not_found_update.append((r[0],))
        else:
            seq_type = type_test(paths[0])
            if seq_type:
                # not empty file
                kma_cmds.append("kma -t_db {0} -i {1} -o {2} -Sparse".format(args.kma_db, paths[0], os.path.join(tmp_folder, r[0])))
                if seq_type == "fasta" and args.sim:
                    # individual genes deserve a lower FDR threshold than 0.05
                    kma_cmds[-1] += " -p 0.20"
                    sim_cmds.append("simreads_multi.py -i {0} -o {1} -z -s".format(r[1], tmp_folder))

                spa_files.append("{}.spa".format(os.path.join(tmp_folder, r[0])))
                path_dict[r[0]] = paths
                type_dict[r[0]] = seq_type
                accessions.append((r[0],r[0]))
            else:
                not_found_update.append((r[0],))

if not_found_update:
    cur.executemany('''UPDATE samples SET included = 404 WHERE accession=?;''', not_found_update)
    conn.commit()

n_jobs = J_LIMIT
if os.environ.get("VU_PARALLEL") is not None:
    n_jobs=min(int(os.environ.get("VU_PARALLEL")), J_LIMIT)

# Run read simulator and kma
if kma_cmds:
    if __name__ == '__main__':

        p = multiprocessing.Pool(n_jobs)
        p.imap_unordered(vu_service.call_silent, kma_cmds)
        p.close()
        p.join()

        if sim_cmds:
            p = multiprocessing.Pool(n_jobs)
            p.imap_unordered(vu_service.call_cmd, sim_cmds)
            p.close()
            p.join()

vu_service.timing(t0, "KMA runs done")

# parse the res files
template_ins = []
aln_pairs = []
for filename in spa_files:
    t, aln_t = get_template(filename, args.id)
    template_ins.extend(t)
    if aln_t is not None:
        aln_pairs.extend(aln_t)

# insert templates into db
if template_ins:
    cur.executemany('''INSERT OR REPLACE INTO templates (accession, template, templ_cov, qc_pass) VALUES (?,?,?,?)''', template_ins)

if not aln_pairs:
    vu_service.stopping("No templates found.")

kma_aln_cmds = []
fastqaftercare_cmds = []
aftercare_cmds = []
osx_cleaning = []
folders = []
newseq_ins = []
trash_files = []
if aln_pairs:
    for aln in aln_pairs:
        folder_path = os.path.join(args.odir, aln[1])
        if folder_path not in folders:
            folders.append(folder_path)
            #aftercare_cmds.append("rm {0}/*.res".format(folder_path))
            #aftercare_cmds.append("rm {0}/*.aln".format(folder_path))
        if not os.path.exists(folder_path):
            os.mkdir(folder_path)
        consensus_file = "{0}/{1}.fsa".format(folder_path, aln[0])
        if not os.path.exists(consensus_file) or not os.path.getsize(consensus_file) > 0:
            if not args.sim or type_dict[aln[0]] != "fasta":
                # fasta
                if type_dict[aln[0]] == "fasta":
                    kma_aln_cmds.append("kma -t_db {0} -i {1} -o {2}/{3} -Mt1 {4} -dense -ref_fsa -nf".format(args.kma_db, path_dict[aln[0]][0], folder_path, aln[0], aln[2]))
                elif type_dict[aln[0]] == "nanopore":
                    kma_aln_cmds.append("kma -t_db {0} -i {1} -o {2}/{3} -Mt1 {4} -mp 25 -bcNano -bc {5} -5p {6} -3p {7} -1t1 -dense -ref_fsa -nf".format(args.kma_db, path_dict[aln[0]][0], folder_path, aln[0], aln[2], config['alignment']['NANOPORE_STRICTNESS'], config['alignment']['TRIM_5P'], config['alignment']['TRIM_3P']))
                else:
                    if len(path_dict[aln[0]]) == 2:
                        # paired reads
                        kma_aln_cmds.append("kma -t_db {0} -ipe {1} -o {2}/{3} -Mt1 {4} -bc 0.9 -5p {5} -3p {6} -1t1 -dense -ref_fsa -cge -nf".format(args.kma_db, " ".join(path_dict[aln[0]]), folder_path, aln[0], aln[2], config['alignment']['TRIM_5P'], config['alignment']['TRIM_3P']))
                    else:
                        # single reads
                        kma_aln_cmds.append("kma -t_db {0} -i {1} -o {2}/{3} -Mt1 {4} -bc 0.9 -1t1 -dense -ref_fsa -cge -nf".format(args.kma_db, path_dict[aln[0]][0], folder_path, aln[0], aln[2]))

            else:
                sim_read = os.path.join(tmp_folder, aln[0])
                kma_aln_cmds.append("kma -t_db {0} -i {1}.fastq.gz -o {2}/{3} -Mt1 {4} -dense -ref_fsa -cge -nf".format(args.kma_db, sim_read, folder_path, aln[0], aln[2]))
            # kma shows uncertainty in lowercase, change to n
            if type_dict[aln[0]] != "fasta":
                fastqaftercare_cmds.append("sed -i -e 's/[actg]/n/g' {0}/{1}.fsa".format(folder_path, aln[0]))
            # replace fasta header with sequence id
            aftercare_cmds.append("sed -i -e 's|^>.*|>{1}|' {0}/{1}.fsa".format(folder_path, aln[0]))
            if sys.platform.startswith('darwin'):
                # OSX
                osx_cleaning.append("rm {0}/{1}.fsa-e".format(folder_path, aln[0]))
            trash_files.append("{0}/{1}.res".format(folder_path, aln[0]))
            trash_files.append("{0}/{1}.aln".format(folder_path, aln[0]))
        newseq_ins.append((aln[0],))

else:
    vu_service.stopping("No templates above threshold found.")

if kma_aln_cmds:
    if __name__ == '__main__':
        p = multiprocessing.Pool(n_jobs)
        p.imap_unordered(vu_service.call_silent, kma_aln_cmds)
        p.close()
        p.join()


    for cmd in fastqaftercare_cmds:
        vu_service.call_shell(cmd)

    aftercare_cmds.extend(batch_trashfiles(trash_files))

    for cmd in aftercare_cmds:
        vu_service.call_shell(cmd)

if osx_cleaning:
    for cmd in osx_cleaning:
        vu_service.call_shell(cmd)

vu_service.timing(t0, "KMA alignments done")

# update database
cur.executemany('''INSERT OR IGNORE INTO sequences(db_id) SELECT db_id from templates where accession=?;''', newseq_ins)
cur.executemany('''UPDATE samples SET included = (SELECT MAX(qc_pass) FROM templates where accession=?) WHERE accession=?;''', accessions)
conn.commit()
conn.close()

# clean up temp folder
try:
    shutil.rmtree(tmp_folder, ignore_errors=True)
except:
    pass

print("Done.", file=sys.stderr)
sys.exit(0)
