#!/usr/bin/env python3

import sys
import os
import time
import subprocess
import shlex
import shutil
import sqlite3
import argparse
from ete3 import Tree
try:
    import cPickle as pickle
except ImportError:
    import pickle
import configparser
import vu_service
from geo_hash import countries_hash, state_hash
from metadata_interpreter import interpret_loc, interpret_date

# increase (pickle) recursion limit
sys.setrecursionlimit(10000)

def SeqsFromFile(filename):
   '''Extract sequences from a file

   Name:
      SeqsFromFile
   Author(s):
      Martin CF Thomsen
   Date:
      18 Jul 2013
   Description:
      Iterator which extract sequence data from the input file
   Args:
      filename: string which contain a path to the input file
   Supported Formats:
      fasta, fastq

   USAGE:
   >>> import os, sys
   >>> # Create fasta test file
   >>> file_content = ('>head1 desc1\nthis_is_seq_1\n>head2 desc2\n'
                       'this_is_seq_2\n>head3 desc3\nthis_is_seq_3\n')
   >>> with open('test.fsa', 'w') as f: f.write(file_content)
   >>> # Parse and print the fasta file
   >>> for seq, name, desc in SeqsFromFile('test.fsa'):
   ...    print ">%s %s\n%s"%(name, desc, seq)
   ...
   >head1 desc1
   this_is_seq_1
   >head2 desc2
   this_is_seq_2
   >head3 desc3
   this_is_seq_3
   '''

   # EXTRACT DATA
   with open_(filename,"r") as f:
      queryseqsegments = []
      seq, name, desc = '', '', ''
      line = ''
      nextline = next(f)
      addsegment = queryseqsegments.append
      for line in f:
         if len(line.strip()) == 0: continue
         #print("%s\n"%line, file=sys.stderr)
         fields=line.strip().split()
         if line[0] == ">":
            # FASTA HEADER FOUND
            if queryseqsegments != []:
               # YIELD SEQUENCE AND RESET
               seq = ''.join(queryseqsegments)
               yield (seq, name, desc)
               seq, name, desc = '', '', ''
               del queryseqsegments[:]
            name = fields[0][1:]
            desc = ' '.join(fields[1:])

         elif line[0] == "@":
            # FASTQ HEADER FOUND
            name = fields[0][1:]
            desc = ' '.join(fields[1:])
            try:
               # EXTRACT FASTQ SEQUENCE
               line = nextline()
               seq  = line.strip().split()[0]
               # SKIP SECOND HEADER LINE AND QUALITY SCORES
               line = nextline()
               line = nextline() # Qualities
            except:
               break
            else:
               # YIELD SEQUENCE AND RESET
               yield (seq, name, desc)
               seq, name, desc = '', '', ''

         elif len(fields[0])>0:
            # EXTRACT FASTA SEQUENCE
            addsegment(fields[0])

      # CHECK FOR LAST FASTA SEQUENCE
      if queryseqsegments != []:
         # YIELD SEQUENCE
         seq = ''.join(queryseqsegments)
         yield (seq, name, desc)

############# FUNCTIONS #############
def open_(filename, mode=None, compresslevel=9):
   """Switch for both open() and gzip.open().

   Determines if the file is normal or gzipped by looking at the file
   extension.

   The filename argument is required; mode defaults to 'rb' for gzip and 'r'
   for normal and compresslevel defaults to 9 for gzip.

   """
   if filename[-3:] == '.gz':
      if mode is None: mode = 'rb'
      return gzip.open(filename, mode, compresslevel)
   else:
      if mode is None: mode = 'r'
      return open(filename, mode)

def input_validation(filename):
    if filename is None:
        vu_service.exiting("Input file not given.")
    if not os.path.exists(filename):
        vu_service.exiting("Input file not given.")
    else:
        return os.path.abspath(filename)

def change2subdir(mode, method, suffix, bdir):
    if mode in ["all", "pw"]:
        subdir = os.path.join(bdir, "{0}_{1}_{2}{3}".format(mode, method, ctime, suffix))
    else:
        subdir = os.path.join(bdir, "{0}_{1}{2}".format(method, ctime, suffix))

    try:
        os.mkdir(subdir)
        os.chdir(subdir)
    except:
        vu_service.exiting("Couldn't make {0}".format(subdir))
    return subdir

def print2msa(msafile, fsafilename, seqname):
    print(">{0}".format(seqname), file=msafile)
    entries = list(zip(*[[seq, name, desc] for seq, name, desc in SeqsFromFile(fsafilename)]))
    print("".join(list(entries[0])), file=msafile)
    return

def print2msa_masked(msafile, fsafilename, seqname, vcf_f):
    print(">{0}".format(seqname), file=msafile)
    entries = list(zip(*[[seq, name, desc] for seq, name, desc in SeqsFromFile(fsafilename)]))
    mask_pos = vcf_mask_positions(vcf_f)
    conc = "".join(list(entries[0]))
    #clean_chunk = []
    concat_start = 0
    valid_until_i = 0
    while valid_until_i < len(mask_pos):
        while concat_start in mask_pos:
            concat_start += 1
        if concat_start > 0:
            valid_until_i = mask_pos.index(concat_start-1) + 1
        if valid_until_i < len(mask_pos):
            concat_end = mask_pos[valid_until_i]
            print(conc[concat_start:concat_end], end="", file=msafile)
            #clean_chunk.append(conc[concat_start:concat_end])
            concat_start = concat_end + 1
            valid_until_i += 1
    #clean_chunk.append(conc[concat_start:])
    print(conc[concat_start:], file=msafile)
    return

def vcf_mask_positions(fp):
    mask_pos = []
    contigs = []
    contig_lens = []
    if os.path.exists(fp) and os.path.getsize(fp) > 0:
        if fp[-3:] == '.gz':
            inputvcf = gzip.open(fp, mode='rt')
        else:
            inputvcf = open(fp, "r")

        for line in inputvcf:
            if line.startswith("##contig"):
                contig_info = line.strip().split("=", 1)[-1][1:-1].split(",")
                contig_id = ""
                contig_len = 0
                for item in contig_info:
                    k, v = item.split("=")
                    if k == "ID":
                        contig_id = v
                    elif k == "length":
                        contig_len = int(v)
                    else:
                        pass
                if contig_id:
                    contigs.append(contig_id)
                    contig_lens.append(contig_len)
            elif not line.startswith("##"):
                cols = line.strip().split("\t")
                # masking where FILTER is "mask"
                if cols[6] == "mask":
                    # convert to zero index
                    concat_i = int(cols[1]) - 1
                    try:
                        cont_no = contigs.index(cols[0])
                        for i in range(cont_no):
                            concat_i += contig_lens[i]
                    except ValueError:
                        vu_service.exiting("{} not in vcf contigs".format(cols[0]))
                    mask_pos.append(concat_i)
    inputvcf.close()
    return mask_pos

def decode_nw_file(seq2name_file, nw_file):
    # load the pickle with id: seqname pairs
    seqid2name = {}
    with open(seq2name_file, "rb") as pf:
        seqid2name = pickle.load(pf)

    newick = []
    with open(nw_file, "r") as nw_f:
        for line in nw_f:
            newick.append(line)

    newick_str = "".join(newick)
    for key in seqid2name.keys():
        newick_str = newick_str.replace(key, seqid2name.get(key))

    with open(nw_file, "w") as nw_f:
        nw_f.write(newick_str)

    return

def decode_dist_matrix(seq2name_file, dist_mat_file, full_dist_mat_file):
    # load the pickle with id: seqname pairs
    seqid2name = {}
    with open(seq2name_file, "rb") as pf:
        seqid2name = pickle.load(pf)

    try:
        mat_f = open(dist_mat_file, "r")
        op = open(full_dist_mat_file, "w")
        print(mat_f.readline(), end="", file=op)
        for line in mat_f:
            if config['tree']['PROGRAM'].lower() == "ccphylo":
                # output only lower diagonal
                line = line.strip().split("\t0", 1)[0] + "\n"
            first_word = line.split()[0]
            try:
                print(line.replace(first_word, seqid2name[first_word]), end="", file=op)
            except KeyError:
                print(line, end="", file=op)
        mat_f.close()
        op.close()
    except IOError as e:
        vu_service.exiting("Matrix decoding failed", notify=True)
    return

def infer_distance_tree(matpath):
    if config['tree']['PROGRAM'].lower() == "neighbor":
        # get the NJ tree made
        inputstr = "{0}\nL\nY\n".format(matpath)
        proc = subprocess.Popen(DTREE, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        proc.stdin.write(inputstr.encode())
        # wait for it. it gets stuck in simple wait() if fails
        (stdoutdata, stderrdata) = proc.communicate()
        if proc.returncode:
            vu_service.exiting("Neighbor program failed.", notify=True)
    elif config['tree']['PROGRAM'].lower() == "ccphylo":
        cmd = "{0} tree -i {1} -o outtree --float_precision".format(DTREE, matpath)
        if os.path.getsize(matpath)/10**9 > int(config['tree']['MAX_RAM']):
            cmd = "{0} tree -i {1} -o outtree --short_precision 100".format(DTREE, matpath, config['tree']['TMP_DIR'])
        ex = vu_service.call_cmd(cmd)
        if ex:
            vu_service.exiting("Ccphylo program failed.", notify=True)
        else:
            sed_cmd = 'sed -i -e \'s|"||g\' outtree'
            ex = vu_service.call_cmd(sed_cmd)
    else:
        vu_service.exiting("Distance based method not recognized.", notify=True)
    return


def refill_tree(treepath, outpath):
    '''Place redundant samples as new nodes with a distance of zero from the cluster representative'''
    try:
        tree = Tree(treepath)
    except NewickError as e:
        vu_service.exiting("Couldn't open {0}".format(treepath))

    # go through the tree and check for clusters in db
    cur.execute('''SELECT count(db_id) from templ_seqs where repr_id is not Null;''')
    if cur.fetchone()[0] > 0:
        full_tree = tree.copy()
        for node in tree.traverse():
            if node.name:
                cur.execute('''SELECT accession from templ_seqs where repr_id=?;''', (node.name,))
                records = cur.fetchall()
                if records:
                    homNode = full_tree.search_nodes(name=node.name)[0]
                    oriNode = homNode.add_child(name=node.name, dist=0)
                    for rec in records:
                        isoNode = homNode.add_child(name="*{}".format(rec[0]), dist=0)
        tree = full_tree

    tree.write(format=0, outfile=outpath)
    return tree

def min_refill_tree(treepath, outpath):
    '''Place redundant samples onto the node of their cluster representative'''
    try:
        tree = Tree(treepath)
    except NewickError as e:
        vu_service.exiting("Couldn't open {0}".format(treepath))

    # go through the tree and check for clusters in db
    cur.execute('''SELECT count(db_id) from templ_seqs where repr_id is not Null;''')
    if cur.fetchone()[0] > 0:
        for node in tree.traverse():
            if node.name:
                cur.execute('''SELECT accession from templ_seqs where repr_id=?;''', (node.name,))
                records = cur.fetchall()
                if records is not None:
                    new_name = [node.name]
                    for rec in records:
                        new_name.append(rec[0])
                    node.name = " ".join(new_name)

    tree.write(format=0, outfile=outpath)
    return tree

def decorate_tree(tree, outpath):
    '''Add metadata to the tips if available'''
    # add metadata if exists
    cur.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='metadata' ''')
    if cur.fetchone()[0] == 1:
        # go through the tree and check for clusters in db
        for node in tree.traverse():
            if node.name:
                rec = []
                if node.name.startswith('*'):
                    cur.execute("select * from metadata where accession=?", (node.name[1:],))
                    rec = cur.fetchone()
                    if rec:
                        node.name = '*{}'.format(" ".join([x.replace(":", ".") for x in rec if x is not None]))
                else:
                    cur.execute("select * from metadata where accession=?", (node.name,))
                    rec = cur.fetchone()
                    if rec:
                        node.name = " ".join([x.replace(":", ".") for x in rec if x is not None])

    tree.write(format=0, outfile=outpath)
    return

# Start time to keep track of progress
t0 = time.time()
ctime = int(t0)

# Parse command line options
parser = argparse.ArgumentParser(
    description='Viral trees')
parser.add_argument(
   '-o',
   dest="odir",
   help='Template directory')
parser.add_argument(
    '-db',
    dest="database",
    help="Sample database")
parser.add_argument(
    '-m',
    dest="distmat",
    default=None,
    help='Distance matrix for NJ')
parser.add_argument(
    '-f',
    dest="filelist",
    default=None,
    help='List of sequences')
parser.add_argument(
    '-d',
    dest="distance",
    action="store_true",
    help='Distance based phylogenic tree')
parser.add_argument(
    '-l',
    dest="likelihood",
    action="store_true",
    help='Maximum likelihood based phylogenic tree')
parser.add_argument(
    '-a',
    dest="allcalled",
    action="store_true",
    help='Mode is "all"')
parser.add_argument(
    '-aln',
    dest="aln",
    action="store_true",
    help='Create or preserve alignments')
parser.add_argument(
    '-mask',
    dest="vcf_file",
    help='Optional VCF file with masked reference positions')
parser.add_argument(
    '-reduce',
    dest="reduce_tips",
    action='store_true',
    help='Put redundant samples onto same tree node')
parser.add_argument(
    '-t', '--debug',
    dest="debug",
    action="store_true",
    help='Debug: use .t suffix')
args = parser.parse_args()

# get confidence parameter from config
config_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config.ini')
if os.path.exists(os.path.join(os.path.realpath(os.path.join(args.odir, "..")), 'config.ini')):
    config_file = os.path.join(os.path.realpath(os.path.join(args.odir, "..")), 'config.ini')
config = configparser.ConfigParser()
config.read(config_file)

## Set variables based on method
DTREE = config['tree']['PROGRAM'].lower()
IQTREE = "iqtree"

suffix = ""
if args.debug:
    suffix = ".t"

if args.allcalled:
    mode = "all"
else:
    mode = "pw"

if args.odir is None:
    vu_service.exiting('Output directory is needed')

if shutil.which(DTREE) is None and shutil.which(IQTREE) is None:
    vu_service.exiting('Neighbor or Iqtree not in path')

# File for distance matrix output
if args.distmat is None:
    if args.allcalled:
        outputmat = os.path.join(args.odir, "dist.all.mat{0}".format(suffix))
    else:
        outputmat = os.path.join(args.odir, "dist.pw.mat{0}".format(suffix))

## Open database
conn = sqlite3.connect(args.database)
conn.execute("PRAGMA foreign_keys = 1")
conn.commit()
cur = conn.cursor()

cur.execute('''CREATE TABLE IF NOT EXISTS matrix
    (db_id INTEGER PRIMARY KEY,
    template TEXT,
    mode TEXT,
    ctime DATETIME DEFAULT CURRENT_TIMESTAMP,
    mpath TEXT,
    UNIQUE(template, mode)
    );''')

cur.execute('''CREATE TABLE IF NOT EXISTS trees
    (db_id INTEGER PRIMARY KEY,
    template TEXT,
    method TEXT,
    mode TEXT,
    ctime DATETIME DEFAULT CURRENT_TIMESTAMP,
    tpath TEXT,
    UNIQUE(template, method, mode)
    );''')

conn.commit()

# create a temp view with the template's samples
templ = os.path.basename(args.odir)
cur.execute('CREATE TEMP VIEW templ_seqs AS SELECT sequences.*,curr_template.* from sequences INNER JOIN ( select * from templates where template="{}") AS curr_template ON sequences.db_id = curr_template.db_id;'.format(templ))
oldseqs = []
cur.execute('''SELECT accession from templ_seqs where repr_id is NULL;''')
rows = cur.fetchall()
if rows is not None:
    for r in rows:
        oldseqs.append("{}.fsa".format(r[0]))

if len(oldseqs) < 3:
    vu_service.stopping("Less than 3 sequences for {}".format(templ))

mode = 'pw'
if args.allcalled:
    mode = 'all'

seq2name_file_path = os.path.join(args.odir, "seqid2name.{}.pic".format(mode))

# stash matrix into db
matrix_file = os.path.join(args.odir, "{0}_{1}_{2}.mat{3}".format(templ, mode, ctime, suffix))
decode_dist_matrix(seq2name_file_path, outputmat, matrix_file)
if os.path.getsize(matrix_file)/10**9 > (int(config['tree']['MAX_RAM']) / 10):
    exit_code = vu_service.call_cmd("gzip {}".format(matrix_file))
    if exit_code:
        vu_service.exiting("Zipping error on matrix.", notify=True)
    else:
        matrix_file = matrix_file + ".gz"

# make alignment
if args.aln and not args.likelihood:
    msa_path = "{}.aln".format(matrix_file.rsplit(".", 1)[0])
    try:
        msafile = open(msa_path, "w")
        for iso in oldseqs:
            longpath = os.path.join(args.odir, iso)
            seqname = iso.split(".")[0]
            if os.path.isfile(longpath):
                print2msa(msafile, longpath, seqname)
        msafile.close()
    except IOError as e:
        vu_service.exiting("File couldn't be opened, {0}.".format(e))

tree_obj = None
if args.distance:
    method = "dist"
    wdir = change2subdir(mode, method, suffix, args.odir)
    matpath = os.path.relpath(outputmat)
    treefilename = "outtree"

    infer_distance_tree(matpath)
    vu_service.timing(t0, "# Distance based tree constructed.")

    decode_nw_file(seq2name_file_path, os.path.join(wdir, treefilename))

    fulltree_file = os.path.join(args.odir, "{0}_{1}_{2}_{3}.nwk{4}".format(templ, mode, method, ctime, suffix))
    if not args.reduce_tips:
        tree_obj = refill_tree(os.path.join(wdir, treefilename), fulltree_file)
        decortree_file = os.path.join(args.odir, "{0}_{1}_{2}_{3}.newick{4}".format(templ, mode, method, ctime, suffix))
        decorate_tree(tree_obj, decortree_file)
    else:
        tree_obj = min_refill_tree(os.path.join(wdir, treefilename), fulltree_file)
        decortree_file = fulltree_file

    cur.execute('''INSERT OR REPLACE INTO trees (template,method,mode,tpath) VALUES (?,?,?,?);''', (templ, method, mode, decortree_file))
    conn.commit()

    vu_service.timing(t0, "# Distance trees saved.")

if args.likelihood:
    method = "ml"
    matpath = os.path.relpath(outputmat)
    if args.filelist == '-':
        isolates = oldseqs
    else:
        filepath = input_validation(args.filelist)

        try:
            pathfile = open(filepath, "r")
            isolates = pathfile.readlines()
            pathfile.close()
        except IOError as e:
            exiting("File couldn't be opened, {0}.".format(e))

    wdir = change2subdir(mode, method, suffix, args.odir)
    treefilename = "sequences.fa.treefile"

    # get the NJ tree made
    infer_distance_tree(matpath)

    decode_nw_file(seq2name_file_path, os.path.join(wdir, "outtree"))

    # make the maxL tree
    try:
        msafile = open("sequences.fa", "w")
    except IOError as e:
        vu_service.exiting("File couldn't be opened, {0}.".format(e))

    # template is not part of it any more!
    for iso in isolates:
        longpath = os.path.join(args.odir, iso.strip())
        seqname = iso.split(".")[0]
        if os.path.isfile(longpath):
            if args.vcf_file is None:
                print2msa(msafile, longpath, seqname)
            else:
                print2msa_masked(msafile, longpath, seqname, args.vcf_file)
    msafile.close()

    if args.aln:
        try:
            shutil.copy("sequences.fa", "{}.aln".format(matrix_file.rsplit(".", 1)[0]))
        except:
            pass

    # construct iqtree cmd
    # /data/evergreen/scripts/iqtree-omp
    # -s Escherichia_coli_K_12_substr__MG1655_uid57779_99.fa -st DNA -m GTR+I+G
    # -nt AUTO -mem 14Gb -bb 1000
    cmd = "{0} -s sequences.fa -st DNA -m GTR+I+G -nt 4 -mem 8Gb -nstop 50 -t outtree".format(IQTREE)
    if len(isolates) > 3:
        cmd += " -bb 1000"
    with open("sequences.fa.out", "w") as ofile:
        p = subprocess.run(shlex.split(cmd), stdout=ofile)
    if p.returncode > 0:
        vu_service.timing(t0, "# WARNING: Iqtree failed.")
    else:
        vu_service.timing(t0, "# Maximum likelihood based tree constructed.")

        fulltree_file = os.path.join(args.odir, "{0}_{1}_{2}_{3}.nwk{4}".format(templ, mode, method, ctime, suffix))
        if not args.reduce_tips:
            tree_obj = refill_tree(os.path.join(wdir, treefilename), fulltree_file)
            decortree_file = os.path.join(args.odir, "{0}_{1}_{2}_{3}.newick{4}".format(templ, mode, method, ctime, suffix))
            decorate_tree(tree_obj, decortree_file)
        else:
            tree_obj = min_refill_tree(os.path.join(wdir, treefilename), fulltree_file)
            decortree_file = fulltree_file

        cur.execute('''INSERT OR REPLACE INTO trees (template,method,mode,tpath) VALUES (?,?,?,?);''', (templ, method, mode, decortree_file))
        conn.commit()

        vu_service.timing(t0, "# Likelihood trees saved.")

# save matrix
cur.execute('''INSERT OR REPLACE INTO matrix (template,mode,mpath) VALUES (?,?,?);''', (templ, mode, matrix_file))
conn.commit()

# create tsv
cur.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='metadata' ''')
if cur.fetchone()[0] == 1 and tree_obj is not None:
    table_name = "metadata"
    # pango lineage data could be included
    cur.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='pango_lineage' ''')
    if cur.fetchone()[0] == 1:
        cur.execute('''CREATE TEMP VIEW metalineage AS SELECT metadata.*,pango_lineage.lineage FROM metadata LEFT JOIN pango_lineage USING(accession);''')
        conn.commit()
        table_name = "metalineage"
    tsv_info = []
    # non-redundant
    cur.execute("select * from {} where accession in (select accession from templ_seqs where repr_id is Null);".format(table_name))
    rows = cur.fetchall()
    if rows is not None:
        for rec in rows:
            tsv_info.append([x.replace('"','').replace(',','') if x is not None else "NA" for x in rec])
            coords = interpret_loc(rec[3])
            if coords['region']:
                tsv_info[-1][3] = "{}:{}".format(coords['country'], coords['region'])
            tsv_info[-1].extend([coords['latitude'], coords['longitude']])

    # redundant
    cur.execute("select * from {} where accession in (select accession from templ_seqs where repr_id is not Null and repr_id!='S');".format(table_name))
    rows = cur.fetchall()
    if rows is not None:
        for rec in rows:
            tsv_info.append([x.replace('"','').replace(',','') if x is not None else "NA" for x in rec])
            if not args.reduce_tips:
                tsv_info[-1][0] = "*" + tsv_info[-1][0]
            coords = interpret_loc(rec[3])
            if coords['region']:
                tsv_info[-1][3] = "{}:{}".format(coords['country'], coords['region'])
            tsv_info[-1].extend([coords['latitude'], coords['longitude']])

    tsv_cont = []
    cur.execute("PRAGMA table_info({});".format(table_name))
    # 0|accession|TEXT|0||1
    # 1|description|TEXT|0||0
    # 2|submit_date|TEXT|0||0
    # 3|country|TEXT|0||0
    # 4|lineage|TEXT|0||0
    columns = ["id"]
    columns.extend(x[1] for x in cur.fetchall()[1:])
    tmp = columns[:2]
    tmp.extend(["year", "month", "day"])
    tmp.extend(["country__autocolor"])
    if table_name != "metadata" and columns[4] == "lineage":
        tmp.extend(["PANGO-lineage__autocolor"])
    tmp.extend(["latitude", "longitude"])
    tsv_cont.append("\t".join(tmp))
    # ['id', 'description', 'year', 'month', 'day', 'country__autocolor', 'PANGO-lineage', 'latitude', 'longitude']
    for lst in tsv_info:
        date = ["","",""]
        if lst[2]:
            date = interpret_date(lst[2])
        tmp = lst[:2]
        tmp.extend(date)
        tmp.extend(lst[3:])
        tsv_cont.append("\t".join(tmp))

    tsv_file = "{}.tsv".format(matrix_file.rsplit(".", 1)[0])
    with open(tsv_file, "w") as op:
        print("\n".join(tsv_cont), file=op)
        print("", file=op)

    vu_service.timing(t0, "# TSV saved.")

conn.close()

# clean up
os.chdir(args.odir)
try:
    shutil.rmtree(wdir)
except:
    pass

print("Done.", file=sys.stderr)
sys.exit(0)
