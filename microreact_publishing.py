#!/usr/bin/env python3

import argparse
import sys
import os
import requests
import json
import time

def load_split_json(file_pointer):
    json_str = "".join(file_pointer.readlines())
    json_str = json_str.split(" = ")[-1]
    return json.loads(json_str)


parser = argparse.ArgumentParser(
    description='Creates microreact projects for tsv of templates and files')
parser.add_argument(
    '-i',
    dest="input",
    help='Input tsv')
parser.add_argument(
    '-o',
    dest="output",
    help='Output tsv')
parser.add_argument(
    '-e',
    dest="email",
    help='Microreact email')
parser.add_argument(
    '-t',
    dest="token",
    help='Microreact token')
args = parser.parse_args()

if args.input is None or not os.path.exists(args.input):
    sys.exit("Input neeeded")

if args.token is None or args.email is None:
    sys.exit("Microreact API access token and/or email needed")

# curl -i -H "Content-type: application/json; charset=UTF-8" -H "access-token: edadd6f00436378a61b262b0543c9b96feedf5c3d93t89107c37dc9e627ca0d2" -X POST -d @project.json https://microreact.org/api/project/

API_URL = "https://microreact.org/api/project"
headers = {'content-type': 'application/json; charset=UTF-8',
           'access-token': args.token }

# template_name1	http://www.host-of-choice.org/folder/template_name1_all_dist_unixtime.nwk
output_obj = []
with open(args.input, "r") as fp:
    for line in fp:
        template, nw = line.strip().split("\t")
        tmp = nw.split("_")
        del tmp[-2]
        unixtime = tmp[-1].split(".")[0]
        tmp[-1] = "{}.tsv".format(unixtime)
        tsv = "_".join(tmp)

        data = {
                "email": args.email,
                "name": template,
                "data": tsv,
                "tree": nw
        }

        response = requests.post(API_URL, data = json.dumps(data), headers = headers)
        if response.status_code == 200:
            d = json.loads(response.content)
            output_obj.append([template, unixtime, d['url']])
        time.sleep(2)

if len(output_obj):
    with open(args.output, "w") as op:
        print("template", "version", "url", sep="\t", file=op)
        for item in output_obj:
            print("\t".join(item), file=op)
