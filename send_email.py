import requests
import os

def send_email(email, subject, html_content):
    return requests.post(
        "{}/messages".format(os.environ.get('EMAIL_DOMAIN')),
        auth=("api", os.environ.get('EMAIL_API_KEY')),
        data={"from": os.environ.get('EMAIL_SENDER'),
              "to": "{}".format(email),
              "subject": str(subject),
              "text": html_content})

