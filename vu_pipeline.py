#!/usr/bin/env python3

import sys, os, time
import argparse
import shutil
import sqlite3
import multiprocessing
import json
import vu_service

def loc_arg_check(param):
    if param is not None and (os.path.exists(param) or os.path.exists("{}.name".format(param))):
        return True
    else:
        return False

parser = argparse.ArgumentParser(
    description='Viral updating phylogeny pipeline')
parser.add_argument(
    '-b',
    dest="bdir",
    default=None,
    help='Base folder for analysis')
parser.add_argument(
    '-d',
    dest="database",
    default=None,
    help='Sample db')
parser.add_argument(
    '-o',
    dest="ofix",
    default=None,
    help='Optional custom name for analysis folder')
parser.add_argument(
    '-q',
    dest="query_file",
    help='Optional query file for update from genbank')
parser.add_argument(
    '-f',
    dest="samples_file",
    default=None,
    help='Optional file with sample ids and paths to fastq or fasta. Files for same sample are comma separated.')
parser.add_argument(
    '-g',
    dest="info_file",
    default=None,
    help='Optional tsv with metadata: accession, description, collection_date, country')
parser.add_argument(
    '-r',
    dest="kma_db",
    default=None,
    help="KMA database, created from reference sequences if doesnt exist")
parser.add_argument(
    '-m',
    dest="mfa_file",
    help='Optional multi-fasta file with reference sequences')
parser.add_argument(
    '-mask',
    dest="vcf_file",
    help='Optional VCF file with masked reference positions')
parser.add_argument(
    '-t',
    dest="id",
    type=float,
    default=90.0,
    help='Template id threshold, sequence space, percentage')
parser.add_argument(
    '-s',
    dest="sim",
    action='store_true',
    help='Simulate reads if templates are much shorter than query')
parser.add_argument(
    '-ml',
    dest="ml",
    action='store_true',
    help='Maximum likelihood tree')
parser.add_argument(
    '-pairwise',
    dest="pw",
    action='store_true',
    help='Pairwise genetic distance calculation')
parser.add_argument(
    '-details',
    dest="details",
    action='store_true',
    help='Output all info from database')
parser.add_argument(
    '-aln',
    dest="aln",
    action='store_true',
    help='Extract consensus alignments')
parser.add_argument(
    '-ebi',
    dest="ebi",
    action='store_true',
    help='EBI compatible output, all files collected in one archive')
parser.add_argument(
    '-ebi_nosummary',
    dest="summary",
    action='store_false',
    help='Dont collect the summary tsv for EBI')
parser.add_argument(
    '-suffix',
    dest="suffix",
    default=None,
    help='Optional custom suffix for outputs')
parser.add_argument(
    '-reduce',
    dest="reduce_tips",
    action='store_true',
    help='Put redundant samples onto same tree node')
parser.add_argument(
    '-again',
    dest="again",
    action='store_true',
    help='Try not included sequences with the new reference')
parser.add_argument(
    '-force',
    dest="force_dist",
    action='store_true',
    help='Force past mapping and alignment')
parser.add_argument(
    '-p',
    dest="cpu",
    type=int,
    default=8,
    help='Number of parallel processes')
args = parser.parse_args()

## MAIN
t0 = time.time()
os.environ['PYTHONUNBUFFERED'] = '1'

suffix = int(t0)
if args.suffix is not None:
    suffix = args.suffix

# check arguments
if not loc_arg_check(args.bdir):
    vu_service.exiting("Parent directory for analysis folder needed")
if not loc_arg_check(args.query_file) and not loc_arg_check(args.database) and not loc_arg_check(args.samples_file):
    vu_service.exiting("Sample query file OR Sample sql database OR Sample file needed")
if not loc_arg_check(args.mfa_file) and not loc_arg_check(args.kma_db):
    vu_service.exiting("Reference fasta file OR reference KMA database needed")

# determine steps
update = False
offline = False
if loc_arg_check(args.query_file):
    update = True
elif loc_arg_check(args.samples_file):
    offline = True
elif not loc_arg_check(args.database):
    vu_service.exiting("Sample query file OR Sample sql database OR Sample file needed")
indexing = False
if loc_arg_check(args.mfa_file) and not loc_arg_check(args.kma_db):
    indexing = True

# set upper limit to procs
cpu_cores = multiprocessing.cpu_count()
args.cpu = min(args.cpu, cpu_cores)

# set paths and env vars
os.environ['VU_PARALLEL'] = str(args.cpu)
if shutil.which('genbank_virus_download.py') is None or shutil.which('vu_mapenaln.py') is None:
    script_dir = os.path.dirname(os.path.realpath(__file__))
    os.environ['PATH'] += ":{}".format(script_dir)
    if shutil.which('vu_mapenaln.py') is None:
        vu_service.exiting("Scripts not in path")

# create folders
args.bdir = os.path.realpath(args.bdir)
ofolder_name = "analysis"
if args.ofix is not None:
    # assumed to be a folder name
    ofolder_name = os.path.basename(args.ofix)
odir = os.path.join(args.bdir, ofolder_name)
if not os.path.exists(odir):
    os.mkdir(odir)

# run genbank module
if update:
    ddir = os.path.join(args.bdir, "data")
    if not os.path.exists(ddir):
        os.mkdir(ddir)
    if args.database is None:
        args.database = "{}.db".format(args.query_file)
    gb_cmd = "genbank_virus_download.py -q {0} -o {1} -d {2}".format(args.query_file, ddir, args.database)
    ex = vu_service.call_cmd(gb_cmd)
    if ex:
        vu_service.exiting("Genbank download unsuccessful")

if offline:
    if args.database is None:
        args.database = "central.db"
    sw_cmd = "sample_wrangler.py -i {0} -d {1}".format(args.samples_file, args.database)
    if loc_arg_check(args.info_file):
        sw_cmd += " -m {}".format(args.info_file)
    ex = vu_service.call_cmd(sw_cmd)
    if ex:
        vu_service.exiting("Samples couldn't be found")

vu_service.timing(t0, "Samples found.")

# calculate k-mer threshold, ie 90% ~ 18.53, 85% ~ 7.43
kmer_id =  round(((args.id/100)**16)*100, 2)

# again option: when new reference was added run kma again against it
if args.again:
    conn = sqlite3.connect(args.database)
    conn.execute("PRAGMA foreign_keys = 1")
    cur = conn.cursor()

    cur.execute('''UPDATE samples SET included=Null WHERE included=0;''')
    conn.commit()
    conn.close()


# run kma index_wrapper
if indexing:
    # create kma db name from mfa name
    if args.kma_db is None:
        kma_db_name = os.path.join(args.bdir, "{}.k16.hr{}".format(os.path.basename(args.mfa_file).rsplit(".", 1)[0], kmer_id))
        args.kma_db = kma_db_name

    # program <multi fasta or pattern> <output prefix> <k> <Sparse prefix> <HR threshold>
    indexing_cmd = "index_wrapper.py {0} {1} 16 - {2}".format(args.mfa_file, args.kma_db, kmer_id)
    #print(indexing_cmd)
    ex = vu_service.call_cmd(indexing_cmd)
    if ex:
        vu_service.exiting("Reference indexing failed")

    vu_service.timing(t0, "Reference indexing done.")

# run vu_mapenaln
mea_cmd = "vu_mapenaln.py -d {0} -o {1} -r {2} -t {3}".format(args.database, odir, args.kma_db, kmer_id)
if args.sim:
    mea_cmd += " -s"
#print(mea_cmd)
ex = vu_service.call_cmd(mea_cmd)
if ex and args.force_dist:
    ex = 0
if ex == 1:
    vu_service.exiting("Mapping and aligning unsuccessful", notify=True)
elif ex == 2:
    vu_service.exiting("No new sequences")

vu_service.timing(t0, "Mapping and aligning done.")

# open database
conn = sqlite3.connect(args.database)
conn.execute("PRAGMA foreign_keys = 1")
cur = conn.cursor()

new_templates = []
cur.execute('''select distinct(template) from templates where db_id in (select db_id from sequences where repr_id="N");''')
rows = cur.fetchall()
if rows is not None:
    for r in rows:
        cur.execute('''select count(accession) from templates where template=?;''', (r[0],))
        if cur.fetchone()[0] > 1:
            new_templates.append(r[0])
conn.close()

# distance calculation mode, true for 'allcalled'
mode_arg = "-a"
if args.pw:
    mode_arg = ""
masking_arg = ""
if args.vcf_file is not None:
    masking_arg = "-mask {}".format(args.vcf_file)


# run virus distance and tree
for templ in new_templates:
    vd_cmd = "vu_distance.py -hr - -n - -o {0} -db {1} {2} {3}".format(os.path.join(odir, templ), args.database, mode_arg, masking_arg)
    #print(vd_cmd)
    ex = vu_service.call_cmd(vd_cmd)
    if ex == 0:
        vu_service.timing(t0, "Distance calculated for {}".format(templ))
        vt_cmd = "vu_trees.py -o {0} -db {1} -f - -d {2} {3}".format(os.path.join(odir, templ), args.database, mode_arg, masking_arg)
        if args.ml:
            vt_cmd += " -l"
        if args.aln:
            vt_cmd += " -aln"
        if args.reduce_tips:
            vt_cmd += " -reduce"
        print(vt_cmd)
        ex = vu_service.call_cmd(vt_cmd)
        if ex == 0:
            vu_service.timing(t0, "Tree inferred for {}".format(templ))
        elif ex == 2:
            vu_service.timing(t0, "Warning: tree unsuccessful for {}".format(templ))
        else:
            vu_service.exiting("Error: tree inference failed for {}".format(templ))
    else:
        vu_service.timing(t0, "Warning: distance unsuccessful for {}".format(templ))

vu_service.timing(t0, "Analysis done")

# summarise results
conn = sqlite3.connect(args.database)
conn.execute("PRAGMA foreign_keys = 1")
cur = conn.cursor()

# update included value for not-included samples
cur.execute('''SELECT DISTINCT(accession) from templates where qc_pass=0 and accession not in (select accession from samples where included=0);''')
rows = cur.fetchall()
if rows is not None:
    accessions = []
    for r in rows:
        accessions.append((r[0],r[0]))
    cur.executemany('''UPDATE samples SET included = (SELECT MAX(qc_pass) FROM templates where accession=?) WHERE accession=?;''', accessions)
    conn.commit()

# check if there are trees
cur.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='trees' ''')
if cur.fetchone()[0] == 1:
    if not args.ebi:
        # create tsv with _updated_ phylogenies
        with open(os.path.join(odir, "phylogenies_{}.tsv".format(suffix)), "w") as op:
            cur.execute('''CREATE TEMP VIEW results AS SELECT trees.*,matrix.* FROM trees LEFT JOIN matrix USING(template);''')
            cur.execute("SELECT template,ctime, mpath, tpath from results where ctime >= datetime(?, 'unixepoch') order by template;", (t0 - 7200,))
            rows = cur.fetchall()
            if rows is not None:
                print("template", "time", "matrix", "tree", sep="\t", file=op)
                if not args.reduce_tips:
                    # microreact won't work with the multiple sample names on the same tip
                    ex_dir = os.path.join(odir, "_external_{}".format(suffix))
                t_dir = os.path.join(odir, "_trees_{}".format(suffix))
                m_dir = os.path.join(odir, "_matrix_{}".format(suffix))
                os.mkdir(ex_dir)
                os.mkdir(t_dir)
                os.mkdir(m_dir)
                if args.aln:
                    aln_dir = os.path.join(odir, "_aln_{}".format(suffix))
                    os.mkdir(aln_dir)
                for r in rows:
                    print("\t".join([x for x in r]), file=op)
                    try:
                        shutil.copy(r[3], t_dir)
                        shutil.copy(r[2], m_dir)
                        if not args.reduce_tips:
                            shutil.copy("{}.nwk".format(r[3].rsplit(".", 1)[0]), ex_dir)
                            shutil.copy("{}.tsv".format(r[2].rsplit(".", 1)[0]), ex_dir)
                        if args.aln:
                            shutil.copy("{}.aln".format(r[2].rsplit(".", 1)[0]), aln_dir)
                    except:
                        pass
    else:
        # create overview tsv and archive of all output
        archive_dir = os.path.join(odir, "phylogenies_{}".format(suffix))
        cur.execute('''CREATE TEMP VIEW allseqs AS SELECT sequences.repr_id,templates.* FROM templates LEFT JOIN sequences USING(db_id);''')
        cur.execute('''CREATE TEMP VIEW files AS SELECT trees.*,matrix.* FROM trees LEFT JOIN matrix USING(template);''')
        cur.execute("SELECT template,ctime, mpath, tpath from files order by template;")
        rows = cur.fetchall()
        if rows is not None:
            os.mkdir(archive_dir)
            for r in rows:
                try:
                    shutil.copy(r[3], archive_dir)
                    shutil.copy(r[2], archive_dir)
                    shutil.copy("{}.nwk".format(r[3].rsplit(".", 1)[0]), archive_dir)
                    shutil.copy("{}.tsv".format(r[2].rsplit(".", 1)[0]), archive_dir)
                    if args.aln:
                        shutil.copy("{}.aln".format(r[2].rsplit(".", 1)[0]), archive_dir)
                except:
                    pass

        if args.summary:
            # if we want a summary file for the EBI style output
            with open(os.path.join(odir, "phylogenies_{}.tsv".format(suffix)), "w") as tsv_fp:
                # chose the better type of tree if available
                method = "dist"
                if args.ml:
                    method = "ml"
                cur.execute("select accession,template,templ_cov,repr_id,ctime,tpath,mpath,qc_pass from allseqs left JOIN files USING(template) where method=?;", (method,))
                rows = cur.fetchall()
                # accession,template,templ_cov,repr_id,ctime,tpath,mpath,qc_pass
                # 0         1           2       3       4      5   6       7
                if rows is not None:
                    print("Seq_ID", "Template","Template_coverage","Repr_ID","Time","Tree", "Matrix", sep="\t", file=tsv_fp)
                    for r in rows:
                        line = [str(x) for x in r[0:4]]
                        # if excluded from tree or too few sequences
                        if r[7] == 0 or r[5] is None:
                            line.extend(["None", "None", "None"])
                        else:
                            line.append(r[4])
                            line.extend([os.path.basename(x) for x in r[5:7]])
                        print("\t".join(line), file=tsv_fp)

        # zip the output dir
        os.chdir(odir)
        exit_code = vu_service.call_cmd("tar -zcf {0}.tar.gz {0}/".format(os.path.basename(archive_dir)))
        if exit_code:
            vu_service.exiting("Zipping error.")

        try:
            shutil.rmtree(archive_dir)
        except:
            vu_service.exiting("Run_output folder couldn't be removed")
else:
    if args.ebi:
        # no trees no files, but there is a sequences table
        cur.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='sequences' ''')
        if cur.fetchone()[0] == 1:
            with open(os.path.join(odir, "phylogenies_{}.tsv".format(suffix)), "w") as tsv_fp:
                cur.execute('''CREATE TEMP VIEW allseqs AS SELECT sequences.repr_id,templates.* FROM templates LEFT JOIN sequences USING(db_id);''')
                cur.execute("select accession,template,templ_cov,repr_id from allseqs;")
                rows = cur.fetchall()
                # accession,template,templ_cov,repr_id
                # 0         1           2       3
                if rows is not None:
                    print("Seq_ID", "Template","Template_coverage","Repr_ID","Time","Tree", "Matrix", sep="\t", file=tsv_fp)
                    for r in rows:
                        line = [str(x) for x in r[0:4]]
                        # no tree
                        line.extend(["None", "None", "None"])
                        print("\t".join(line), file=tsv_fp)

if args.details and not args.ebi:
    det_path = shutil.which("details.sql")
    #print(det_path)
    if det_path is not None:
        dump_cmd = "sqlite3 {} '.read {}'".format(args.database, det_path)
        ex = vu_service.call_cmd(dump_cmd)
        prefix = "_analysis_{}".format(suffix)
        try:
            shutil.move("abc123.included.tsv", os.path.join(odir, "{}.included.tsv".format(prefix)))
            shutil.move("abc123.not_included.tsv", os.path.join(odir, "{}.not_included.tsv".format(prefix)))
            shutil.move("abc123.template.tsv", os.path.join(odir, "{}.template.tsv".format(prefix)))
        except FileExistsError:
            pass

conn.close()
vu_service.timing(t0, "Summary made")



print("DONE", file=sys.stderr)
sys.exit(0)
