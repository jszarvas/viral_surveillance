CREATE TEMP VIEW namedsamples AS SELECT samples.*,metadata.* FROM samples INNER JOIN metadata USING(accession);
CREATE TEMP VIEW fullinfo AS SELECT templates.*,namedsamples.* FROM templates LEFT JOIN namedsamples USING(accession);
.mode tabs
.output abc123.template.tsv
select template,count(accession) from fullinfo where qc_pass=1 group by template;
.output abc123.included.tsv
select accession,description,dl_date,templ_cov,template from fullinfo where included=1;
.output abc123.not_included.tsv
select accession,description,dl_date,templ_cov,template from fullinfo where included=0;
.output stdout
