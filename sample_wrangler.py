#!/usr/bin/env python3

import os
import sys
import argparse
import configparser
from datetime import datetime
import json
import sqlite3
import re
import locale
import time
import vu_service

# set to US for the date parsing
locale.setlocale(locale.LC_ALL, 'en_US.utf-8')

def index_col(header_cols, colname):
    col_i = None
    try:
        col_i = header_cols.index(colname)
    except ValueError:
        pass
    return col_i

parser = argparse.ArgumentParser(
    description='Prepares offline samples for processing')
parser.add_argument(
    '-i',
    dest="samples_file",
    help='Path to file with samples')
parser.add_argument(
    '-m',
    dest="info_file",
    help='Optional path to sample metadata')
parser.add_argument(
    '-d',
    dest="database",
    help="Sample database"
)
args = parser.parse_args()

t0 = time.time()

#start up the db during first run_id
conn = sqlite3.connect(args.database)
conn.execute("PRAGMA foreign_keys = 1")
cur = conn.cursor()

cur.execute('''CREATE TABLE IF NOT EXISTS samples
    (accession TEXT PRIMARY KEY,
    path TEXT,
    feature_path TEXT,
    dl_date TEXT DEFAULT CURRENT_DATE,
    included INTEGER DEFAULT NULL);''')
cur.execute('''CREATE TABLE IF NOT EXISTS metadata
    (accession TEXT PRIMARY KEY,
    description TEXT,
    submit_date TEXT,
    country TEXT);''')
conn.commit()

sample_insert = []
extracted_fasta_path = None
if args.samples_file is not None:
    with open(args.samples_file, "r") as fp:
        for line in fp:
            # sample_id    pathtoread1,pathtoread2
            tmp = line.strip().split("\t")
            if tmp:
                sid = tmp[0]
                fasta_path = tmp[1]
                sample_insert.append((sid, fasta_path, extracted_fasta_path))

    # insert to db
    cur.executemany('''INSERT OR REPLACE INTO samples (accession, path, feature_path) VALUES (?,?,?)''', sample_insert)
    conn.commit()

    vu_service.timing(t0, "Samples saved to database")

else:
    vu_service.exiting("Paths to input files are missing.")

metadata_insert = []
acc_i = None
date_i = None
loc_i = None
descr_i = None
if args.info_file is not None:
    with open(args.info_file, "r") as fp:
        header = fp.readline().strip().split("\t")
        acc_i = index_col(header, "accession")
        date_i = index_col(header, "collection_date")
        loc_i = index_col(header, "country")
        descr_i = index_col(header, "description")

        if acc_i is None:
            vu_service.stopping("Accession column not found")

        for line in fp:
            cols = line.strip("\n").split("\t")
            md = [None, None, None, None]
            if cols[0]:
                for i, ind in enumerate([acc_i, descr_i, date_i, loc_i]):
                    if ind is not None:
                        md[i] = cols[ind]
                metadata_insert.append(tuple(md))

    cur.executemany('''INSERT OR IGNORE INTO metadata (accession, description, submit_date, country) VALUES (?,?,?,?)''', metadata_insert)
    conn.commit()

conn.close()

vu_service.timing(t0, "Metadata saved to database")
