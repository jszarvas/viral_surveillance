#!/usr/bin/env python3

import os
import sys
import argparse
import configparser
from Bio import Entrez
from Bio import SeqIO
from Bio.Seq import Seq
try:
    from Bio.Seq import UnknownSeq
    from Bio.Seq import UndefinedSequenceError
except ImportError:
    pass
from Bio.SeqRecord import SeqRecord
import urllib
from urllib.error import HTTPError
from datetime import datetime
import json
import sqlite3
import re
import locale
import vu_service

BATCH_SIZE = 50
# set to US for the date parsing
locale.setlocale(locale.LC_ALL, 'en_US.utf-8')

def fit_folder(folder, bdir):
    """Avoid having more than 10000 files in one folder."""
    full_folder = os.path.join(bdir, folder)
    i = 1
    while len(os.listdir(full_folder)) > 10000:
        i += 1
        full_folder = os.path.join(bdir, "{0}-{1}".format(folder, i))
        if not os.path.exists(full_folder):
            os.mkdir(full_folder)
    return full_folder

parser = argparse.ArgumentParser(
    description='Makes Entrez query to Genbank, downloads queried gb and extracted nucleotide fasta')
parser.add_argument(
    '-q',
    dest="cf",
    help='Path to the query file')
parser.add_argument(
    '-o',
    dest="out",
    help='Data output directory absolute path')
parser.add_argument(
    '-d',
    dest="database",
    help="Sample database"
)
args = parser.parse_args()

config_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'config.ini')
config = configparser.ConfigParser()
config.read(config_file)

odir = os.path.realpath(args.out)
base = os.path.dirname(odir)
folder = os.path.basename(odir)
odir = fit_folder(folder, base)


#start up the db during first run_id
conn = sqlite3.connect(args.database)
conn.execute("PRAGMA foreign_keys = 1")
cur = conn.cursor()

cur.execute('''CREATE TABLE IF NOT EXISTS samples
    (accession TEXT PRIMARY KEY,
    path TEXT,
    feature_path TEXT,
    dl_date TEXT DEFAULT CURRENT_DATE,
    included INTEGER DEFAULT NULL);''')
cur.execute('''CREATE TABLE IF NOT EXISTS metadata
    (accession TEXT PRIMARY KEY,
    description TEXT,
    submit_date TEXT,
    country TEXT);''')
conn.commit()

queries = []
if args.cf is not None:
    if not os.path.exists(args.cf):
        vu_service.exiting("Queries file doesnt exist.")
    else:
        with open(args.cf, "r") as fp:
            for line in fp:
                #ORF2   txid11983[Organism] AND biomol_genomic[PROP] AND ("450"[SLEN] : "8000"[SLEN])
                tmp = line.strip().split("\t")
                if not tmp[0].isspace():
                    tmp[0] = [x.lower() for x in tmp[0].split("|")]
                    queries.append(tmp)
else:
    vu_service.exiting("No query supplied")

print(queries)

# use Entrez for searching genbank
if config['Entrez']['NCBI_API_KEY'].find("@") != "api_key":
    Entrez.api_key = config['Entrez']['NCBI_API_KEY']
else:
    vu_service.exiting("Edit config.ini for NCBI API access")

if config['Entrez']['NCBI_EMAIL'].find("@") > -1:
    Entrez.email = config['Entrez']['NCBI_EMAIL']
else:
    vu_service.exiting("Edit config.ini for NCBI API access")

for query in queries:
    query_str = query[1]
    # 'txid11983[Organism] AND biomol_genomic[PROP] AND ("450"[SLEN] : "8000"[SLEN])'
    handle = Entrez.esearch(db="nucleotide", term=query_str, idtype="acc", retmax=50000)
    record = Entrez.read(handle)
    handle.close()

    print(query_str, int(record["Count"]))

    acc = []
    for r in record['IdList']:
        cur.execute('''SELECT * FROM samples WHERE accession=?''', (re.sub(r"\W+","_", r),))
        if cur.fetchone() is None:
            acc.append(r)

    n_acc = len(acc)
    batches = [acc[x:x + BATCH_SIZE] for x in range(0, n_acc, BATCH_SIZE)]

    for batch in batches:
        acc_str = ",".join(batch)
        done = False
        while not done:
            sample_insert = []
            metadata_insert = []
            try:
                fetch_handle = Entrez.efetch(db="nuccore", id=acc_str, rettype="gb", retmode="text")
                records = SeqIO.parse(fetch_handle, "gb")
                for r in records:
                    if isinstance(r.seq, UnknownSeq):
                        continue
                    else:
                        try:
                            _ = r.seq[0]
                        except UndefinedSequenceError:
                            continue
                    extracted_fasta_path = None
                    r.id = re.sub(r"\W+","_", r.id)
                    for f in r.features:
                        # save metadata in json
                        if f.type == "source":
                            country = None
                            if f.qualifiers.get('country') is not None:
                                country = f.qualifiers.get('country')[0]
                            sdate = datetime.strftime(datetime.strptime(r.annotations['date'], "%d-%b-%Y"), "%Y-%m-%d")
                            metadata = {'accession': r.id,
                            'description': r.description,
                            'submit_date': sdate }
                            if f.qualifiers.get('collection_date') is not None:
                                sdate = str(f.qualifiers.get('collection_date')[0])
                            metadata_insert.append((r.id, r.description, sdate, country))
                            metadata.update(f.qualifiers)
                            with open(os.path.join(odir, "{}.json".format(r.id)), "w") as op:
                                json.dump(metadata, op)
                        # correct ORF or gene name
                        if query[0][0] != "-":
                            if f.type == "gene" or f.type == "CDS":
                                if (f.qualifiers.get('gene') is not None and f.qualifiers.get('gene')[0].lower() in query[0]) or (f.qualifiers.get('product') is not None and f.qualifiers.get('product')[0].lower() in query[0]) or (f.qualifiers.get('note') is not None and f.qualifiers.get('note')[0].lower() in query[0]):
                                    feature_record = SeqRecord(f.extract(r.seq), id=r.id, description = r.description)
                                    extracted_fasta_path = os.path.join(odir, "{}_{}.fsa".format(query[0][0].upper(), r.id))
                                    SeqIO.write(feature_record, extracted_fasta_path, "fasta")
                        else:
                            break
                    fasta_path = os.path.join(odir, "{}.fsa".format(r.id))
                    SeqIO.write(r, fasta_path, "fasta")
                    sample_insert.append((r.id, fasta_path, extracted_fasta_path))
                fetch_handle.close()

                done = True

                # insert to db
                cur.executemany('''INSERT OR REPLACE INTO samples (accession, path, feature_path) VALUES (?,?,?)''', sample_insert)
                cur.executemany('''INSERT OR REPLACE INTO metadata (accession, description, submit_date, country) VALUES (?,?,?,?)''', metadata_insert)
                conn.commit()

            except ConnectionResetError:
                print("Sleep 120sec on connection reset")
                time.sleep(120)
            except HTTPError as e:
                vu_service.exiting("NCBI returned {}, try again later".format(e.code))

conn.close()
